<?php

/**
 * Class Lease_Library_BaseCRUDController
 */
class Lease_Library_BaseCRUDController
    extends Lease_Library_BaseController
        implements BAS_Shared_Lease_Interface_Crud_Controller
{
    public $crudTitle = 'crud';

    public function init()
    {
        parent::init();

        $this->ajaxContext
            ->setActionContexts([
                'index' => 'json',
                'add' => 'json',
                'edit' => 'json',
                'delete' => 'json',
            ]);

        $this->_includeAMD();
        $this->view->assign('title', $this->crudTitle);
    }

    /**
     * @param BAS_Shared_Lease_Interface_Crud_Service $service
     * @param Lease_Form_Base $form
     * @throws Lease_Library_ResponseException
     * @throws Zend_Form_Exception
     * @throws Zend_View_Exception
     */
    public function baseCreate(BAS_Shared_Lease_Interface_Crud_Service $service, Lease_Form_Base $form)
    {
        $form->setOptions(['actionUrls' => $this->actionUrls]);
        $form->initElements();

        if (!$this->getRequest()->isXmlHttpRequest()) {
            $this->view->assign(['form' => $form]);
            return;
        }

        if (!$form->isValid($this->getAllParams())) {
            $this->throwResponseException([
                'message' => $this->view->translate('validation_error'),
                'form' => (string) $form,
            ], BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY);
        }

        $newModel = $service->saveForm($form->getValues());
        $this->view->assign([
            'form' => (string) $form,
            'message' => $this->view->translate('created'),
            'redirectUrl' => $this->getActionUrl('edit', true) . '/id/' . $newModel->getId(),
        ]);
    }

    /**
     * @param BAS_Shared_Lease_Interface_Crud_Service $service
     * @param Lease_Form_Base $form
     * @throws Zend_View_Exception
     */
    public function baseRead(BAS_Shared_Lease_Interface_Crud_Service $service, Lease_Form_Base $form)
    {
        $form->setOptions(['actionUrls' => $this->actionUrls]);
        $form->initElements();

        $id = (int) $this->getRequest()->getParam('id', 0);

        $form->populate($service->getViewFormData($id));
        $this->view->assign(['form' => $form]);
    }

    /**
     * @param BAS_Shared_Lease_Interface_Crud_Service $service
     * @param Lease_Form_Base $form
     * @throws Lease_Library_ResponseException
     * @throws Zend_Form_Exception
     * @throws Zend_View_Exception
     */
    public function baseUpdate(BAS_Shared_Lease_Interface_Crud_Service $service, Lease_Form_Base $form)
    {
        $id = (int) $this->getRequest()->getParam('id', 0);

        $form->setOptions(['actionUrls' => $this->actionUrls]);
        $form->initElements();

        if (!$this->getRequest()->isXmlHttpRequest()) {
            $form->populate($service->getEditFormData($id));
            $this->view->assign(['form' => $form]);
            return;
        }

        if (!$form->isValid($this->getAllParams())) {
            $this->throwResponseException([
                'message' => $this->view->translate('validation_error'),
                'form' => (string) $form,
            ], BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY);
        }

        $model = $service->saveForm($form->getValues());
        $form->populate($service->getEditFormData($model->getId()));

        $this->view->assign([
            'form' => (string) $form,
            'message' => $this->view->translate('update'),
        ]);
    }

    /**
     * @param BAS_Shared_Lease_Interface_Crud_Service $service
     * @throws Lease_Library_ResponseException
     * @throws Zend_View_Exception
     */
    public function baseDelete(BAS_Shared_Lease_Interface_Crud_Service $service)
    {
        $id = (int) $this->getRequest()->getParam('delete-id', 0);
        $deleted = $service->deleteById($id);

        if ($deleted === 0) {
            $this->throwResponseException([
                'message' => $this->view->translate('delete_fail') . ' id[' . $id . ']',
            ], BAS_Shared_Http_StatusCode::NOT_FOUND);
        }

        $this->view->assign([
            'message' => $this->view->translate('delete_success'),
            'gridAjaxUrl' => $this->_getGridAjaxUrl(),
        ]);
    }

    /**
     * @param Lease_Service_BaseGrid $grid
     * @throws Zend_View_Exception
     */
    public function baseList(Lease_Service_BaseGrid $grid)
    {
        $this->view->assign([
            'actionUrls' => $this->actionUrls,
            'grid' => $grid->getGrid(),
        ]);
    }
}