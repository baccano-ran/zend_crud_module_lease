<?php

class Lease_Library_FormElementBehavior_Hidden implements Lease_Library_FormElementBehavior_Base
{
    public function apply(Zend_Form_Element $element)
    {
        $element
            ->removeDecorator('DtDdWrapper')
            ->removeDecorator('Label')
            ->getDecorator('HtmlTag')->setOption('class', 'form-hidden_field')
        ;
    }
}
