<?php

/**
 * Class Lease_Library_FormElementBehavior_DateTime
 */
class Lease_Library_FormElementBehavior_DateTime implements Lease_Library_FormElementBehavior_Base
{
    private static $_filterDT;

    public function apply(Zend_Form_Element $element)
    {
        if (!self::$_filterDT) {
            self::$_filterDT = new Lease_Form_Filter_DateTimeFormat();
        }
        $element->addFilter(self::$_filterDT);
    }
}
