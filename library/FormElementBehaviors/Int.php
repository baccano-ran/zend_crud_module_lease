<?php

/**
 * Class Lease_Library_FormElementBehavior_Int
 */
class Lease_Library_FormElementBehavior_Int implements Lease_Library_FormElementBehavior_Base
{
    public function apply(Zend_Form_Element $element, $options = [])
    {
        $element->addValidator(new Zend_Validate_Int());
        if (array_key_exists('max', $options)) {
            if (!array_key_exists('min', $options)) {
                $options['min'] = 0;
            }
            $element->addValidator(new Zend_Validate_Between($options));
        }
    }
}
