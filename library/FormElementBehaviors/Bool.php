<?php

/**
 * Class Lease_Library_FormElementBehavior_Bool
 */
class Lease_Library_FormElementBehavior_Bool implements Lease_Library_FormElementBehavior_Base
{
    private static $_filterYN;

    public function apply(Zend_Form_Element $element)
    {
        if (!self::$_filterYN) {
            self::$_filterYN = new Lease_Form_Filter_YesNo();
        }
        $element->addFilter(self::$_filterYN);
    }
}
