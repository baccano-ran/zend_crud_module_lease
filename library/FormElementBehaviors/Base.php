<?php

interface Lease_Library_FormElementBehavior_Base
{
    public function apply(Zend_Form_Element $element);
}
