<?php

/**
 * Class Lease_Library_FormElementBehavior_String
 */
class Lease_Library_FormElementBehavior_String implements Lease_Library_FormElementBehavior_Base
{
    public function apply(Zend_Form_Element $element, $options = [])
    {
        if (isset($options['max'])) {
            $element->setAttrib('maxlength', $options['max']);
        }

        $element->addValidator(new Zend_Validate_StringLength($options));
    }
}
