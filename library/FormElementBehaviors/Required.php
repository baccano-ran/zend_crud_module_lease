<?php

class Lease_Library_FormElementBehavior_Required implements Lease_Library_FormElementBehavior_Base
{
    public function apply(Zend_Form_Element $element)
    {
        $element
            ->setRequired()
            ->getDecorator('Label')->setOption('class', 'form-element-label form-required_field')
        ;
    }
}
