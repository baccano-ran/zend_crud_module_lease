<?php

/**
 * Class Lease_Library_FormElementBehavior_Float
 */
class Lease_Library_FormElementBehavior_Float implements Lease_Library_FormElementBehavior_Base
{
    protected function _getCurrentDecimalSeparator()
    {
        return Lease_Service_BaseService::getDecimalSeparator();
    }

    protected function _getCurrentLocale()
    {
        return Lease_Service_BaseService::getCurrentLocale();
    }

    private static $_floatFilter;
    protected function _getFloatFilter()
    {
        if (!self::$_floatFilter) {
            self::$_floatFilter = new Lease_Form_Filter_Float($this->_getCurrentDecimalSeparator());
        }

        return self::$_floatFilter;
    }

    private static $_floatValidator;
    protected function _getFloatValidator()
    {
        if (!self::$_floatValidator) {
            self::$_floatValidator = new Zend_Validate_Float($this->_getCurrentLocale());
        }

        return self::$_floatValidator;
    }

    public function apply(Zend_Form_Element $element)
    {
        $element
            ->addFilter($this->_getFloatFilter())
            ->addValidator($this->_getFloatValidator())
        ;
    }
}
