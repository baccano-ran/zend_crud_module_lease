<?php

class Lease_Library_Formats
{
    const INTERBANK = 1;

    public static $templates = [
        self::INTERBANK => '{name} ({rate})'
    ];

    /**
     * @param string $format
     * @param string $name
     * @param string|int|float $rate
     * @return string
     */
    public static function format($format, $name, $rate)
    {
        return strtr(
            self::$templates[$format],
            [
                '{name}' => $name,
                '{rate}' => $rate,
            ]
        );
    }
}