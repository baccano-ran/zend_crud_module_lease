<?php

/**
 * Class Lease_BaseController
 * @property Zend_View $view
 * @method Zend_Controller_Request_Http getRequest
 */
class Lease_Library_BaseController extends BAS_Shared_Controller_Action_Abstract
{
    const GRID_DEFAULT_PARAMS = 'gridmod1/ajax/_zfgid/1';
    const TEMPLATE_DIR = '_templates';

    /** @var Zend_Controller_Action_Helper_AjaxContext */
    public $ajaxContext;
    /** @var array */
    public $actionUrls;

    public function init()
    {
        parent::init();
        $id = $this->getRequest()->getParam('id');
        $id = $id ? '/id/' . $id : '';

        $this->ajaxContext = $this->_helper->getHelper('AjaxContext');
        $this->actionUrls = [
            'index' => $this->getActionUrl('index', true),
            'add' => $this->getActionUrl('add', true),
            'edit' => $this->getActionUrl('edit', true) . $id,
            'view' => $this->getActionUrl('view', true) . $id,
            'delete' => $this->getActionUrl('delete', true) . $id,
        ];

        $this->_initDefaultActionTemplate();
    }

    /**
     * @throws Zend_View_Exception
     */
    protected function _initDefaultActionTemplate()
    {
        $fullViewPath = $this->view->getScriptPath(
            $this->_request->getControllerName() . '/' .
            $this->_request->getActionName() . '.phtml'
        );

        if (file_exists($fullViewPath)) return;

        /** @var Zend_Controller_Action_Helper_ViewRenderer $viewRenderer */
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        $viewRenderer
            ->setView($this->view)
            ->setNoController(true)
        ;

        $this->view->addScriptPath($this->view->getScriptPath(null) . self::TEMPLATE_DIR);
    }

    /**
     * @param string $action
     * @param bool|false $resetParams
     * @return string mixed
     */
    public function getActionUrl($action, $resetParams = false)
    {
        return $this->view->url([
            'module'     => $this->getRequest()->getModuleName(),
            'controller' => $this->getRequest()->getControllerName(),
            'action'     => $action,
        ], null, $resetParams);
    }

    public function getActiveDepot()
    {
        return BAS_Shared_Auth_Service::getActiveDepotId();
    }

    /**
     * @return Zend_Config
     */
    protected function getBptConfig()
    {
        /** @var Zend_Config $config */
        $config = $this->getInvokeArg('bootstrap')->getResource('config');
        return $config->get('lease');
    }

    /**
     * @param null|mixed $data
     * @param null|int $statusCode
     * @param string $errorMessage
     * @throws Lease_Library_ResponseException
     * @throws Zend_Controller_Response_Exception
     */
    public function throwResponseException($data = null, $statusCode = null, $errorMessage = '')
    {
        if ($statusCode !== null) {
            $this->getResponse()->setHttpResponseCode($statusCode);
        }

        $exception = new Lease_Library_ResponseException($errorMessage);
        $exception->setResponseData($data);
        throw $exception;
    }

    protected function _includeAMD()
    {
        $this->view->headScript()
            ->setAllowArbitraryAttributes(true)
            ->appendFile(
                $this->view->baseUrl('/js/lease/vendor/require.js'),
                'text/javascript',
                ['data-main' => $this->view->baseUrl('/js/lease/init')]
            )
        ;
    }

    protected function _getGridAjaxUrl()
    {
        return
            $this->_request->getModuleName() . '/' .
            $this->_request->getControllerName() . '/' .
            'index/' . static::GRID_DEFAULT_PARAMS
            ;
    }
}
