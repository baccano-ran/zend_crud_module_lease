<?php

/**
 * Class Lease_Library_ResponseException
 */
class Lease_Library_ResponseException extends BAS_Shared_Exception
{
    public $_responseData;

    /**
     * @param array $data
     */
    public function setResponseData($data)
    {
        $this->_responseData = $data;
    }

    /**
     * @return mixed
     */
    public function getResponseData()
    {
        return $this->_responseData;
    }
}
