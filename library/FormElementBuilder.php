<?php

/**
 * Class Lease_Library_FormElementBuilder
 */
class Lease_Library_FormElementBuilder
{
    public $behaviorPrefix = 'Lease_Library_FormElementBehavior_';

    protected $_elements = [];
    protected $_behaviors = [];

    /** @var Zend_Form */
    public $form;

    /**
     * Lease_Library_FormElementBuilder constructor.
     * @param Zend_Form $form
     */
    public function __construct(Zend_Form $form)
    {
        $this->form = $form;
    }

    /**
     * @param string $name
     * @return Zend_Form_Element
     */
    public function getElement($name)
    {
        return $this->_elements[$name];
    }

    /**
     * @return Zend_Form_Element[]
     */
    public function getElements()
    {
        return $this->_elements;
    }

    /**
     * @param $behavior
     * @param array $options
     * @return Lease_Library_FormElementBehavior_Base
     */
    public function getBehavior($behavior, $options = [])
    {
        if (is_numeric($behavior) && is_string($options)) {
            $behClName = $this->behaviorPrefix . ucfirst($options);
        } else {
            $behClName = $this->behaviorPrefix . ucfirst($behavior);
        }

        if (!array_key_exists($behClName, $this->_behaviors)) {
            $this->_behaviors[$behClName] = new $behClName;
        }

        return $this->_behaviors[$behClName];

    }

    /**
     * @param Zend_Form_Element $element
     * @param array $behaviors
     * @return $this
     */
    public function addElement(Zend_Form_Element $element, $behaviors = [])
    {
        $this->_elements[$element->getName()] = $element;

        foreach ($behaviors as $behavior => $options) {
            $this->getBehavior($behavior, $options)->apply($element, $options);
        }

        return $this;
    }

    /**
     * @param string $type
     * @param string $fieldName
     * @param array $options
     * @param array $behaviors
     * @return $this
     * @throws Zend_Form_Exception
     */
    public function createElement($type, $fieldName, $options = [], $behaviors = [])
    {
        $element = $this->form->createElement($type, $fieldName, $options);
        $this->_elements[$fieldName] = $element;

        foreach ($behaviors as $behavior => $options) {
            $this->getBehavior($behavior, $options)->apply($element, $options);
        }

        return $this;
    }
}
