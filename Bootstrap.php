<?php

/**
 * Default module bootsrap
 * @package Default
 */
class Lease_Bootstrap extends Zend_Application_Module_Bootstrap
{

    /**
     * Init namespace
     */
    public function _initNamespaces()
    {
        $resourceLoader = $this->getResourceLoader();

        $resourceLoader
            ->addResourceType('library', 'library/', 'Library')
            ->addResourceType('library_grid', 'library/grid/', 'Library_Grid')
            ->addResourceType('form_element_builder',
                'library/FormElementBehaviors/', 'Library_FormElementBehavior')
            ->addResourceType('service', 'services/', 'Service')

            ->addResourceType('form', 'forms/', 'Form')
            ->addResourceType('form_element', 'forms/elements/', 'Form_Element')
            ->addResourceType('form_filter', 'forms/filters/', 'Form_Filter')

            ->addResourceType('form_interbank', 'forms/interbank/', 'Form_Interbank')
            ->addResourceType('form_configuration', 'forms/configuration/', 'Form_Configuration')
            ->addResourceType('form_option', 'forms/option/', 'Form_Option')
            ->addResourceType('form_option_tenor', 'forms/option-tenor/', 'Form_OptionTenor')
            ->addResourceType('form_option_max_downpayment', 'forms/option-max-downpayment/', 'Form_OptionMaxDownpayment')
            ->addResourceType('form_raiffeisen_contact', 'forms/raiffeisen-contact/', 'Form_RaiffeisenContact')
            ->addResourceType('form_administration_cost', 'forms/administration-cost/', 'Form_AdministrationCost')
            ->addResourceType('form_fee_category', 'forms/fee-category/', 'Form_FeeCategory')
            ->addResourceType('form_fee', 'forms/fee/', 'Form_Fee')
            ->addResourceType('form_price', 'forms/price/', 'Form_Price')
            ->addResourceType('form_interest', 'forms/interest/', 'Form_Interest')
            ->addResourceType('form_email_template', 'forms/offer-email-template/', 'Form_OfferEmailTemplate')
            ->addResourceType('form_management_cost', 'forms/management-cost/', 'Form_ManagementCost')
            ->addResourceType('form_vat_rate', 'forms/vat-rate/', 'Form_VatRate')
        ;
    }

    /**
     * Initialize action helpers
     */
    public function _initActionHelpers()
    {
    }
}
