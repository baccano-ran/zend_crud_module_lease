<?php

/**
 * Class Lease_FeeController
 */
class Lease_FeeController extends Lease_Library_BaseCRUDController
{
    /** @var Lease_Service_Fee */
    public $service;
    public $crudTitle = 'fee';

    public function init()
    {
        parent::init();
        $this->ajaxContext->initContext();
        $this->service = new Lease_Service_Fee();
    }

    public function indexAction()
    {
        parent::baseList(new Lease_Service_FeeGrid(
            $this->service->getGridSelect($this->getActiveDepot()),
            $this->view,
            $this->actionUrls
        ));
    }

    public function addAction()
    {
        $form = new Lease_Form_Fee_Create([
            'feeCategoryActionUrls' => $this->_getFeeCategoryUrls(),
        ]);
        parent::baseCreate($this->service, $form);
    }

    public function editAction()
    {
        $form = new Lease_Form_Fee_Update([
            'feeCategoryActionUrls' => $this->_getFeeCategoryUrls(),
        ]);
        parent::baseUpdate($this->service, $form);
    }

    public function viewAction()
    {
        parent::baseRead(
            $this->service,
            new Lease_Form_Fee_View()
        );
    }

    public function deleteAction()
    {
        parent::baseDelete($this->service);
    }

    /**
     * @return array
     */
    protected function _getFeeCategoryUrls()
    {
        $feeAddUrl = $this->view->url([
            'module'     => $this->getRequest()->getModuleName(),
            'controller' => 'fee-category',
            'action'     => 'add',
        ], null, true);

        $feeEditUrl = $this->view->url([
            'module'     => $this->getRequest()->getModuleName(),
            'controller' => 'fee-category',
            'action'     => 'edit',
        ], null, true);

        return [
            'add' => $feeAddUrl,
            'edit' => $feeEditUrl,
        ];
    }
}
