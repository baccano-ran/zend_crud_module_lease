<?php

/**
 * Class Lease_OfferEmailTemplateController
 */
class Lease_OfferEmailTemplateController extends Lease_Library_BaseCRUDController
{
    /** @var Lease_Service_OfferEmailTemplate */
    public $service;
    public $crudTitle = 'offer_email_template';

    public function init()
    {
        parent::init();
        $this->ajaxContext->initContext();
        $this->service = new Lease_Service_OfferEmailTemplate();
    }

    public function indexAction()
    {
        parent::baseList(new Lease_Service_OfferEmailTemplateGrid(
            $this->service->getGridSelect($this->getActiveDepot()),
            $this->view,
            $this->actionUrls
        ));
    }

    public function addAction()
    {
        parent::baseCreate(
            $this->service,
            new Lease_Form_OfferEmailTemplate_Create()
        );
    }

    public function editAction()
    {
        parent::baseUpdate(
            $this->service,
            new Lease_Form_OfferEmailTemplate_Update()
        );
    }

    public function viewAction()
    {
        parent::baseRead(
            $this->service,
            new Lease_Form_OfferEmailTemplate_View()
        );
    }

    public function deleteAction()
    {
        parent::baseDelete($this->service);
    }
}
