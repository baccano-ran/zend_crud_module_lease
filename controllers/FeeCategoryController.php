<?php

/**
 * Class Lease_FeeCategoryController
 */
class Lease_FeeCategoryController extends Lease_Library_BaseCRUDController
{
    /** @var Lease_Service_FeeCategory */
    public $service;
    public $crudTitle = 'fee_category';

    public function init()
    {
        parent::init();
        $this->ajaxContext->initContext();
        $this->service = new Lease_Service_FeeCategory();
    }

    public function indexAction()
    {
        parent::baseList(new Lease_Service_FeeCategoryGrid(
            $this->service->getGridSelect(),
            $this->view,
            $this->actionUrls
        ));
    }

    public function addAction()
    {
        parent::baseCreate(
            $this->service,
            new Lease_Form_FeeCategory_Create()
        );
    }

    public function editAction()
    {
        parent::baseUpdate(
            $this->service,
            new Lease_Form_FeeCategory_Create()
        );
    }

    public function viewAction()
    {
        parent::baseRead(
            $this->service,
            new Lease_Form_FeeCategory_View()
        );
    }

    public function deleteAction()
    {
        parent::baseDelete($this->service);
    }
}
