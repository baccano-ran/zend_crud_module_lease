<?php

/**
 * Class Lease_ManagementCostController
 */
class Lease_ManagementCostController extends Lease_Library_BaseCRUDController
{
    /** @var Lease_Service_ManagementCost() */
    public $service;
    public $crudTitle = 'management_cost';

    public function init()
    {
        parent::init();
        $this->ajaxContext->initContext();
        $this->service = new Lease_Service_ManagementCost();
    }

    public function indexAction()
    {
        parent::baseList(new Lease_Service_ManagementCostGrid(
            $this->service->getGridSelect($this->getActiveDepot()),
            $this->view,
            $this->actionUrls
        ));
    }

    public function addAction()
    {
        parent::baseCreate(
            $this->service,
            new Lease_Form_ManagementCost_Create()
        );
    }

    public function editAction()
    {
        parent::baseUpdate(
            $this->service,
            new Lease_Form_ManagementCost_Update()
        );
    }

    public function viewAction()
    {
        parent::baseRead(
            $this->service,
            new Lease_Form_ManagementCost_View()
        );
    }

    public function deleteAction()
    {
        parent::baseDelete($this->service);
    }
}
