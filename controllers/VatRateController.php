<?php

/**
 * Class Lease_VatRateController
 */
class Lease_VatRateController extends Lease_Library_BaseCRUDController
{
    /** @var Lease_Service_VatRate */
    public $vatRateService;
    public $crudTitle = 'vat_rate';

    public function init()
    {
        parent::init();
        $this->ajaxContext->initContext();
        $this->vatRateService = new Lease_Service_VatRate();
    }

    public function indexAction()
    {
        parent::baseList(new Lease_Service_VatRateGrid(
            $this->vatRateService->getGridSelect($this->getActiveDepot()),
            $this->view,
            $this->actionUrls
        ));
    }

    public function addAction()
    {
        parent::baseCreate(
            $this->vatRateService,
            new Lease_Form_VatRate_Create()
        );
    }

    public function editAction()
    {
        parent::baseUpdate(
            $this->vatRateService,
            new Lease_Form_VatRate_Update()
        );
    }

    public function viewAction()
    {
        parent::baseRead(
            $this->vatRateService,
            new Lease_Form_VatRate_View()
        );
    }

    public function deleteAction()
    {
        parent::baseDelete($this->vatRateService);
    }

}
