<?php

/**
 * Class Lease_ErrorController
 */
class Lease_ErrorController extends ErrorController
{
    /** @var  Zend_Controller_Action_Helper_AjaxContext */
    public $ajaxContext;

    public function init()
    {
        $this->ajaxContext = $this->_helper->getHelper('AjaxContext');
        $this->ajaxContext
            ->addActionContexts([
                'error' => 'json',
            ])
            ->initContext();
    }

    public function errorAction()
    {
        $error = $this->_getParam('error_handler');

        /** @var \Exception $exception */
        $exception = $error->exception;

        if ($exception instanceof Lease_Library_ResponseException) {
            /** @var Lease_Library_ResponseException $exception */
            $this->view->clearVars();
            $this->view->assign($exception->getResponseData());
            return;
        }

        parent::errorAction();
    }
}