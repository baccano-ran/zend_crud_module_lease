<?php
/**
 * Class Lease_OptionController
 */
class Lease_OptionController extends Lease_Library_BaseCRUDController
{
    /** @var Lease_Service_Option */
    public $optionService;
    public $crudTitle = 'options';

    /**
     * Initialization
     */
    public function init()
    {
        parent::init();
        $this->ajaxContext
            ->addActionContext('index', 'json')
            ->initContext();

        $this->optionService = new Lease_Service_Option();
    }

    /**
     * Option grid.
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest() || $this->getParam('_zfgid')) {

            $confId = (int) $this->getParam('lease_configurations_id');
            $grid = new Lease_Service_OptionGrid(
                $this->optionService->getGridSelectByConfigId(
                    $this->getActiveDepot(), $confId),
                $this->view,
                $this->actionUrls
            );

            $grid = $grid->getGrid();
            $this->view->assign(['grid' => (string) $grid, 'script' => $grid->getJSCode()]);
            return;
        }

        parent::baseList(new Lease_Service_OptionGrid(
            $this->optionService->getGridSelect($this->getActiveDepot()),
            $this->view,
            $this->actionUrls
        ));
    }

    /**
     * Add option
     */
    public function addAction()
    {
        $form = new Lease_Form_Option_Create([
            'leaseConfigurationsId' => $this->getParam('id'),
        ]);
        parent::baseCreate($this->optionService, $form);
    }

    /**
     * Edit option
     */
    public function editAction()
    {
        $newItemUrl = $this->view->url([
            'module'     => $this->getRequest()->getModuleName(),
            'controller' => 'option-tenor',
            'action'     => 'add',
        ]);

        $newOptionMaxDownpaymentUrl = $this->view->url([
            'module'     => $this->getRequest()->getModuleName(),
            'controller' => 'option-max-downpayment',
            'action'     => 'add',
        ]);

        $form = new Lease_Form_Option_Update([
            'optionGridUrl' => $this->_getOptionGridUrl(),
            'newItemUrl' => $newItemUrl,
            'optionMaxDownpaymentGridUrl' => $this->_getOptionMaxDownpaymentGridUrl(),
            'newOptionMaxDownpaymentUrl' => $newOptionMaxDownpaymentUrl
        ]);

        parent::baseUpdate($this->optionService, $form);
    }

    /**
     * View option
     */
    public function viewAction()
    {
        $form = new Lease_Form_Option_View([
            'optionGridUrl' => $this->_getOptionGridUrl(),
        ]);

        parent::baseUpdate($this->optionService, $form);
    }

    /**
     * Delete option
     */
    public function deleteAction()
    {
        parent::baseDelete($this->optionService);
    }

    /**
     * Get option grid url.
     *
     * @return string
     */
    protected function _getOptionGridUrl()
    {
        $id = (int) $this->getParam('id');

        return $this->view->url([
            'module'     => $this->getRequest()->getModuleName(),
            'controller' => 'option-tenor',
            'action'     => 'index',
            'format'     => 'json',
            'lease_options_id' => $id,
        ], null, true);
    }

    /**
     * Get lease option max downpayment url.
     *
     * @return string
     */
    protected function _getOptionMaxDownpaymentGridUrl()
    {
        $id = (int)$this->getParam('id');

        return $this->view->url([
            'module'     => $this->getRequest()->getModuleName(),
            'controller' => 'option-max-downpayment',
            'action'     => 'index',
            'format'     => 'json',
            'lease_options_id' => $id,
        ], null, true);
    }
}
