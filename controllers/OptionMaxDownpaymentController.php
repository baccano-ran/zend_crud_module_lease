<?php
/**
 * Class Lease_OptionMaxDownpaymentController
 */
class Lease_OptionMaxDownpaymentController extends Lease_Library_BaseCRUDController
{
    /** @var Lease_Service_OptionMaxDownpayment $optionMaxDownpaymentService */
    public $optionMaxDownpaymentService;

    /** @var string $crudTitle */
    public $crudTitle = 'option_max_downpayment';

    /**
     * Initialization
     */
    public function init()
    {
        parent::init();
        $this->ajaxContext
            ->addActionContext('index', 'json')
            ->initContext();

        $this->optionMaxDownpaymentService = new Lease_Service_OptionMaxDownpayment();
    }

    /**
     * Option max downpayment list.
     */
    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest() || $this->getParam('_zfgid')) {
            $optionId = (int)$this->getParam('lease_options_id');
            $grid = new Lease_Service_OptionMaxDownpaymentGrid(
                $this->optionMaxDownpaymentService->getGridSelectByOptionId($optionId),
                $this->view,
                $this->actionUrls
            );
            $grid = $grid->getGrid();
            $this->view->assign([
                'grid' => (string) $grid,
                'script' => $grid->getJSCode(),
            ]);

            return;
        }

        parent::baseList(new Lease_Service_OptionMaxDownpaymentGrid(
            $this->optionMaxDownpaymentService->getGridSelect(),
            $this->view,
            $this->actionUrls
        ));
    }

    /**
     * Option max downpayment add form.
     */
    public function addAction()
    {
        $form = new Lease_Form_OptionMaxDownpayment_Create([
            'leaseOptionsId' => $this->getParam('id'),
        ]);
        parent::baseCreate($this->optionMaxDownpaymentService, $form);
    }

    /**
     * Option max downpayment edit form.
     */
    public function editAction()
    {
        $newItemUrl = $this->view->url([
            'module'     => $this->getRequest()->getModuleName(),
            'controller' => 'option-max-downpayment',
            'action'     => 'add',
        ]);

        $form = new Lease_Form_OptionMaxDownpayment_Update([
            'optionGridUrl' => $this->_getGridUrl(),
            'newItemUrl' => $newItemUrl,
        ]);

        parent::baseUpdate($this->optionMaxDownpaymentService, $form);
    }

    /**
     * Option max downpayment view form
     */
    public function viewAction()
    {
        $form = new Lease_Form_OptionMaxDownpayment_View([
            'optionGridUrl' => $this->_getGridUrl(),
        ]);

        parent::baseUpdate($this->optionMaxDownpaymentService, $form);
    }

    /**
     * Option max downpayment delete
     */
    public function deleteAction()
    {
        parent::baseDelete($this->optionMaxDownpaymentService);
    }

    /**
     * Get grid url.
     *
     * @return string
     */
    protected function _getGridUrl()
    {
        $id = (int) $this->getParam('id');

        return $this->view->url([
            'module'     => $this->getRequest()->getModuleName(),
            'controller' => 'option-max-downpayment',
            'action'     => 'index',
            'format'     => 'json',
            'lease_options_id' => $id,
        ], null, true);
    }
}
