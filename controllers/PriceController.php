<?php

/**
 * Class Lease_PriceController
 */
class Lease_PriceController extends Lease_Library_BaseCRUDController
{
    /** @var Lease_Service_Price */
    public $service;
    public $crudTitle = 'price';

    public function init()
    {
        parent::init();
        $this->ajaxContext->initContext();
        $this->service = new Lease_Service_Price();
    }

    public function indexAction()
    {
        parent::baseList(new Lease_Service_PriceGrid(
            $this->service->getGridSelect(),
            $this->view,
            $this->actionUrls
        ));
    }

    public function addAction()
    {
        parent::baseCreate(
            $this->service,
            new Lease_Form_Price_Create()
        );
    }

    /**
     * @param string $source
     * @return array
     */
    protected function _parseId($source)
    {
        $ids = [
            'legacy_VoertuigID' => 1,
            'country_code' => 'NL',
            'tenor' => 1
        ];
        $params = explode('_', $source);

        foreach ($ids as $key => $defaultValue) {
            if (($param = each($params)) === false) break;
            list(, $ids[$key]) = $param;
        }

        return $ids;
    }

    public function editAction()
    {
        $id = $this->_parseId($this->getParam('id'));

        $form = new Lease_Form_Price_Update();
        $form->setOptions(['actionUrls' => $this->actionUrls]);
        $form->initElements();

        if (!$this->getRequest()->isXmlHttpRequest()) {
            $form->populate($this->service->getEditFormData($id));
            $this->view->assign(['form' => $form]);
            return;
        }

        if (!$form->isValid($this->getAllParams())) {
            $this->throwResponseException([
                'message' => $this->view->translate('validation_error'),
                'form' => (string) $form,
            ], BAS_Shared_Http_StatusCode::UNPROCESSABLE_ENTITY);
        }

        $model = $this->service->saveForm($form->getValues());
        $form->populate($this->service->getEditFormData($id));

        $this->view->assign([
            'form' => (string) $form,
            'message' => $this->view->translate('update'),
        ]);
    }

    public function viewAction()
    {
        $id = $this->_parseId($this->getParam('id'));

        $form = new Lease_Form_Price_View();
        $form->setOptions(['actionUrls' => $this->actionUrls]);
        $form->initElements();

        $form->populate($this->service->getViewFormData($id));
        $this->view->assign(['form' => $form]);
    }

    public function deleteAction()
    {
        $id = $this->_parseId($this->getParam('delete-id'));
        $deleted = $this->service->deleteById($id);

        if ($deleted === 0) {
            $this->throwResponseException([
                'message' => $this->view->translate('delete_fail') . ' id[' . $id . ']',
            ], BAS_Shared_Http_StatusCode::NOT_FOUND);
        }

        $this->view->assign([
            'message' => $this->view->translate('delete_success'),
            'gridAjaxUrl' => $this->_getGridAjaxUrl(),
        ]);
    }
}
