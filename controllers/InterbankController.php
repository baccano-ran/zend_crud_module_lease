<?php

/**
 * Class Lease_InterbankController
 */
class Lease_InterbankController extends Lease_Library_BaseCRUDController
{
    public $crudTitle = 'interbank';
    /** @var Lease_Service_InterbankRate */
    public $service;

    public function init()
    {
        $this->service = new Lease_Service_InterbankRate();
        parent::init();
        $this->ajaxContext->initContext();
    }

    public function indexAction()
    {
        parent::baseList(new Lease_Service_InterbankRateGrid(
            $this->service->getGridSelect($this->getActiveDepot()),
            $this->view,
            $this->actionUrls
        ));
    }

    public function addAction()
    {
        parent::baseCreate(
            $this->service,
            new Lease_Form_Interbank_Create()
        );
    }

    public function editAction()
    {
        parent::baseUpdate(
            $this->service,
            new Lease_Form_Interbank_Update()
        );
    }

    public function viewAction()
    {
        parent::baseRead(
            $this->service,
            new Lease_Form_Interbank_View()
        );
    }

    public function deleteAction()
    {
        parent::baseDelete($this->service);
    }
}
