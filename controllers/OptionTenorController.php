<?php

/**
 * Class Lease_OptionTenorController
 */
class Lease_OptionTenorController extends Lease_Library_BaseCRUDController
{
    /** @var Lease_Service_OptionTenor */
    public $optionTenorService;
    public $crudTitle = 'option_tenor';

    public function init()
    {
        parent::init();
        $this->ajaxContext->initContext();
        $this->optionTenorService = new Lease_Service_OptionTenor();
    }

    public function indexAction()
    {
        if ($this->getRequest()->isXmlHttpRequest() || $this->getParam('_zfgid')) {

            $optionId = (int) $this->getParam('lease_options_id');
            $grid = new Lease_Service_OptionTenorGrid(
                $this->optionTenorService->getGridSelectByOptionId($optionId),
                $this->view,
                $this->actionUrls
            );

            $grid = $grid->getGrid();
            $this->view->assign(['grid' => (string) $grid, 'script' => $grid->getJSCode()]);
            return;
        }

        parent::baseList(new Lease_Service_OptionTenorGrid(
            $this->optionTenorService->getGridSelect(),
            $this->view,
            $this->actionUrls
        ));
    }

    public function addAction()
    {
        $form = new Lease_Form_OptionTenor_Create(['leaseOptionsId' => $this->getParam('id')]);
        parent::baseCreate($this->optionTenorService, $form);
    }

    public function editAction()
    {
        parent::baseUpdate(
            $this->optionTenorService,
            new Lease_Form_OptionTenor_Update()
        );
    }

    public function viewAction()
    {
        parent::baseRead(
            $this->optionTenorService,
            new Lease_Form_OptionTenor_View()
        );
    }

    public function deleteAction()
    {
        parent::baseDelete($this->optionTenorService);
    }

}
