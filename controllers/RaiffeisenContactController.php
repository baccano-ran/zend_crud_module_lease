<?php

/**
 * Class Lease_RaiffeisenContactController
 */
class Lease_RaiffeisenContactController extends Lease_Library_BaseCRUDController
{
    /** @var Lease_Service_RaiffeisenContact */
    public $raiffeisenContactService;
    public $crudTitle = 'raiffeisen_contact';

    public function init()
    {
        parent::init();
        $this->ajaxContext->initContext();
        $this->raiffeisenContactService = new Lease_Service_RaiffeisenContact();
    }

    public function indexAction()
    {
        parent::baseList(new Lease_Service_RaiffeisenContactGrid(
            $this->raiffeisenContactService->getGridSelect($this->getActiveDepot()),
            $this->view,
            $this->actionUrls
        ));
    }

    public function addAction()
    {
        parent::baseCreate(
            $this->raiffeisenContactService,
            new Lease_Form_RaiffeisenContact_Create()
        );
    }

    public function editAction()
    {
        parent::baseUpdate(
            $this->raiffeisenContactService,
            new Lease_Form_RaiffeisenContact_Update()
        );
    }

    public function viewAction()
    {
        parent::baseRead(
            $this->raiffeisenContactService,
            new Lease_Form_RaiffeisenContact_View()
        );
    }

    public function deleteAction()
    {
        parent::baseDelete($this->raiffeisenContactService);
    }

}
