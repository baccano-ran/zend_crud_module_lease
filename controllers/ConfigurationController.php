<?php

/**
 * Class Lease_ConfigurationController
 */
class Lease_ConfigurationController extends Lease_Library_BaseCRUDController
{
    /** @var Lease_Service_Configuration */
    public $confService;
    public $crudTitle = 'configuration';

    public function init()
    {
        parent::init();
        $this->ajaxContext->initContext();
        $this->confService = new Lease_Service_Configuration();
    }

    public function indexAction()
    {
        parent::baseList(new Lease_Service_ConfigurationGrid(
            $this->confService->getGridSelect($this->getActiveDepot()),
            $this->view,
            $this->actionUrls
        ));
    }

    public function addAction()
    {
        parent::baseCreate(
            $this->confService,
            new Lease_Form_Configuration_Create()
        );
    }

    public function editAction()
    {
        $newItemUrl = $this->view->url([
            'module'     => $this->getRequest()->getModuleName(),
            'controller' => 'option',
            'action'     => 'add',
        ]);

        $form = new Lease_Form_Configuration_Update([
            'optionGridUrl' => $this->_getOptionGridUrl(),
            'newItemUrl' => $newItemUrl,
        ]);

        parent::baseUpdate($this->confService, $form);
    }

    public function viewAction()
    {
        $form = new Lease_Form_Configuration_View([
            'optionGridUrl' => $this->_getOptionGridUrl(),
        ]);

        parent::baseRead($this->confService, $form);
    }

    public function deleteAction()
    {
        parent::baseDelete(new Lease_Service_Configuration());
    }

    /**
     * @return string
     */
    protected function _getOptionGridUrl()
    {
        $id = (int) $this->getParam('id');

        return $this->view->url([
            'module'     => $this->getRequest()->getModuleName(),
            'controller' => 'option',
            'action'     => 'index',
            'format'     => 'json',
            'lease_configurations_id' => $id,
        ], null, true);
    }

}
