<?php

/**
 * Class Lease_InterestController
 */
class Lease_InterestController extends Lease_Library_BaseCRUDController
{
    /** @var Lease_Service_Interest */
    public $service;
    public $crudTitle = 'interest';

    public function init()
    {
        parent::init();
        $this->ajaxContext->initContext();
        $this->service = new Lease_Service_Interest();
    }

    public function indexAction()
    {
        parent::baseList(new Lease_Service_InterestGrid(
            $this->service->getGridSelect($this->getActiveDepot()),
            $this->view,
            $this->actionUrls
        ));
    }

    public function addAction()
    {
        parent::baseCreate(
            $this->service,
            new Lease_Form_Interest_Create()
        );
    }

    public function editAction()
    {
        parent::baseUpdate(
            $this->service,
            new Lease_Form_Interest_Update()
        );
    }

    public function viewAction()
    {
        parent::baseRead(
            $this->service,
            new Lease_Form_Interest_View()
        );
    }

    public function deleteAction()
    {
        parent::baseDelete($this->service);
    }
}
