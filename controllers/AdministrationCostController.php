<?php

/**
 * Class Lease_AdministrationCostController
 */
class Lease_AdministrationCostController extends Lease_Library_BaseCRUDController
{
    /** @var Lease_Service_AdministrationCost */
    public $service;
    public $crudTitle = 'administration_cost';

    public function init()
    {
        parent::init();
        $this->ajaxContext->initContext();
        $this->service = new Lease_Service_AdministrationCost();
    }

    public function indexAction()
    {
        parent::baseList(new Lease_Service_AdministrationCostGrid(
            $this->service->getGridSelect($this->getActiveDepot()),
            $this->view,
            $this->actionUrls
        ));
    }

    public function addAction()
    {
        parent::baseCreate(
            $this->service,
            new Lease_Form_AdministrationCost_Create()
        );
    }

    public function editAction()
    {
        parent::baseUpdate(
            $this->service,
            new Lease_Form_AdministrationCost_Update()
        );
    }

    public function viewAction()
    {
        parent::baseRead(
            $this->service,
            new Lease_Form_AdministrationCost_View()
        );
    }

    public function deleteAction()
    {
        parent::baseDelete($this->service);
    }
}
