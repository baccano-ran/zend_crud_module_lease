<?php

/**
 * Class Lease_Form_AdministrationCost_View
 */
class Lease_Form_AdministrationCost_View extends Lease_Form_BaseView
{
    /** @var Lease_Service_Configuration */
    public $configurationService;
    public $floatFields = [
        'fixed',
        'rate',
    ];

    /**
     * @return array
     */
    protected function _getElements()
    {
        $euroFilter = new Lease_Form_Filter_Template(['before' => '&euro; ']);

        $id = new Zend_Form_Element_Hidden('id');

        $countryCode = new Zend_Form_Element_Note('country_name');
        $countryCode
            ->setLabel('country');

        $fixed = new Zend_Form_Element_Note('fixed');
        $fixed
            ->setLabel('fixed')
            ->addFilter($euroFilter)
        ;

        $rate = new Zend_Form_Element_Note('rate');
        $rate
            ->setLabel('rate')
            ->addFilter($euroFilter)
        ;

        $importantNote = new Zend_Form_Element_Note('important_note');
        $importantNote->setValue(
            'Important!: Leave rate empty if you want fixed value of administration costs'
        );

        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
            ->setLabel('updated_at')
        ;

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy
            ->setLabel('updated_by');

        return [
            $id,
            $countryCode,
            $fixed,
            $rate,
            $importantNote,
            $updatedAt,
            $updatedBy,
        ];
    }

}