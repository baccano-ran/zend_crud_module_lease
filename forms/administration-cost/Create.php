<?php

/**
 * Class Lease_Form_AdministrationCost_Create
 */
class Lease_Form_AdministrationCost_Create extends Lease_Form_BaseCreate
{
    public $mainGroup = 'administration_cost';

    /** @var Lease_Service_AdministrationCost */
    public $service;
    public $floatFields = [
        'fixed',
        'rate',
    ];

    public function initElements()
    {
        $this->service = new Lease_Service_AdministrationCost();
        parent::initElements();
    }

    /**
     * @return string[]
     */
    protected function _getCountryOptions()
    {
        return ['' => 'none'] + $this->service->getCountryOptions();
    }

    /**
     * @return Zend_Form_Element[]
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');
        
        $countryCode = new Zend_Form_Element_Select('countryCode');
        $countryCode
            ->setMultiOptions($this->_getCountryOptions())
            ->setLabel('country')
            ->setRequired()
            ->getDecorator('Label')->setOption('class', 'form-element-label form-required_field')
        ;

        $fixed = new Zend_Form_Element_Text('fixed');
        $fixed
            ->setLabel('fixed')
        ;

        $rate = new Zend_Form_Element_Text('rate');
        $rate
            ->setLabel('rate')
        ;

        $importantNote = new Zend_Form_Element_Note('important_note');
        $importantNote->setValue(
            'Important!: Leave rate empty if you want fixed value of administration costs'
        );

        return [
            $id,
            $countryCode,
            $fixed,
            $rate,
            $importantNote,
        ];
    }

}