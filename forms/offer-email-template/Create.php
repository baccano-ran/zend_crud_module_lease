<?php

/**
 * Class Lease_Form_OfferEmailTemplate_Create
 */
class Lease_Form_OfferEmailTemplate_Create extends Lease_Form_BaseCreate
{
    public $mainGroup = 'email_template';
    public $requiredFields = [
        'countryCode',
        'subject',
        'body',
    ];

    /**
     * @var Lease_Service_OfferEmailTemplate()
     */
    public $emailTemplateService;

    public function initElements()
    {
        $this->emailTemplateService = new Lease_Service_OfferEmailTemplate();
        parent::initElements();
    }

    protected function getCountries()
    {
        return BAS_Shared_Utils_Array::listData(
            $this->emailTemplateService->getAvailableCountries(),
            'code',
            'name'
        );
    }

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $country = new Zend_Form_Element_Select('countryCode');
        $country
            ->setMultiOptions($this->getCountries())
            ->setLabel('country')
        ;

        $subject = new Zend_Form_Element_Text('subject');
        $subject->setLabel('subject');

        $body = new Zend_Form_Element_Textarea('body');
        $body->setLabel('body');

        $version = new Zend_Form_Element_Text('version');
        $version->setLabel('version');

        $active = new Zend_Form_Element_Checkbox('active');
        $active->setLabel('active');

        return [
            $id,
            $country,
            $subject,
            $body,
            $version,
            $active,
        ];
    }

}