<?php

/**
 * Class Lease_Form_OfferEmailTemplate_View
 */
class Lease_Form_OfferEmailTemplate_View extends Lease_Form_BaseView
{
    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $country = new Zend_Form_Element_Note('country_name');
        $country->setLabel('country');

        $subject = new Zend_Form_Element_Note('subject');
        $subject->setLabel('subject');

        $body = new Zend_Form_Element_Note('body');
        $body->setLabel('body');

        $version = new Zend_Form_Element_Note('version');
        $version->setLabel('version');

        $active = new Zend_Form_Element_Note('active');
        $active
            ->addFilter(new Lease_Form_Filter_YesNo())
            ->setLabel('active')
        ;

        $createdAt = new Zend_Form_Element_Note('createdAt');
        $createdAt
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
            ->setLabel('created_at')
        ;

        $createdBy = new Zend_Form_Element_Note('creator_full_name');
        $createdBy->setLabel('created_by');

        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
            ->setLabel('updated_at')
        ;

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy->setLabel('updated_by');

        return [
            $id,
            $country,
            $subject,
            $body,
            $version,
            $active,
            $createdAt,
            $createdBy,
            $updatedAt,
            $updatedBy,
        ];
    }

}