<?php

/**
 * Class Lease_Form_RaiffeisenContact_Update
 */
class Lease_Form_RaiffeisenContact_Update extends Lease_Form_RaiffeisenContact_Create
{
    protected function _getElements()
    {
        $createdAt = new Zend_Form_Element_Note('createdAt');
        $createdAt
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
            ->setLabel('created_at');

        $createdBy = new Zend_Form_Element_Note('creator_full_name');
        $createdBy->setLabel('created_by');

        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
            ->setLabel('updated_at');

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy->setLabel('updated_by');

        return array_merge(parent::_getElements(), [
            $createdAt,
            $createdBy,
            $updatedAt,
            $updatedBy,
        ]);
    }
}
