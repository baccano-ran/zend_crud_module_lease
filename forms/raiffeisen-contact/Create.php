<?php

/**
 * Class Lease_Form_RaiffeisenContact_Create
 */
class Lease_Form_RaiffeisenContact_Create extends Lease_Form_BaseCreate
{
    public $mainGroup = 'raiffeisen_contact';

    public $requiredFields = [
        'countryCode',
        'fullName',
        'email',
    ];
    /**
     * @return array
     */
    public function getCountryOptions()
    {
        $service = new Lease_Service_Configuration();
        return ['' => 'none'] + $service->getActiveConfCountryOptions();
    }

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');
        
        $countryCode = new Zend_Form_Element_Select('countryCode');
        $countryCode
            ->addMultiOptions($this->getCountryOptions())
            ->setLabel('country')
        ;

        $fullName = new Zend_Form_Element_Text('fullName');
        $fullName
            ->setLabel('fullname')
        ;

        $email = new Zend_Form_Element_Text('email');
        $email
            ->setLabel('email')
        ;

        $phone = new Zend_Form_Element_Text('phone');
        $phone
            ->setLabel('phone')
        ;

        $mobile = new Zend_Form_Element_Text('mobile');
        $mobile
            ->setLabel('mobile')
        ;

        $fax = new Zend_Form_Element_Text('fax');
        $fax
            ->setLabel('fax')
        ;

        return [
            $id,
            $countryCode,
            $fullName,
            $email,
            $phone,
            $mobile,
            $fax,
        ];
    }

}