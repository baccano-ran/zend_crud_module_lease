<?php

/**
 * Class Lease_Form_RaiffeisenContact_View
 */
class Lease_Form_RaiffeisenContact_View extends Lease_Form_BaseView
{
    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $countryName = new Zend_Form_Element_Note('country_name');
        $countryName
            ->setLabel('country')
        ;

        $fullName = new Zend_Form_Element_Note('fullName');
        $fullName
            ->setLabel('fullname')
        ;

        $filterMail = new Lease_Form_Filter_Template();
        $filterMail->setTemplate('<a href="mailto:{value}" class="mailto">{value}</a>');

        $email = new Zend_Form_Element_Note('email');
        $email
            ->addFilter($filterMail)
            ->setLabel('email')
        ;

        $phone = new Zend_Form_Element_Note('phone');
        $phone
            ->setLabel('phone')
        ;

        $mobile = new Zend_Form_Element_Note('mobile');
        $mobile
            ->setLabel('mobile')
        ;

        $fax = new Zend_Form_Element_Note('fax');
        $fax
            ->setLabel('fax')
        ;

        $createdAt = new Zend_Form_Element_Note('createdAt');
        $createdAt
            ->setLabel('created_at')
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
        ;

        $createdBy = new Zend_Form_Element_Note('creator_full_name');
        $createdBy
            ->setLabel('created_by')
        ;

        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt
            ->setLabel('updated_at')
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
        ;

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy
            ->setLabel('updated_by')
        ;

        return [
            $id,
            $countryName,
            $fullName,
            $email,
            $phone,
            $mobile,
            $fax,
            $createdAt,
            $createdBy,
            $updatedAt,
            $updatedBy,
        ];
    }

}