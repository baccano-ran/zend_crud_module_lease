<?php

/**
 * Class Lease_Form_ManagementCost_View
 */
class Lease_Form_ManagementCost_View extends Lease_Form_BaseView
{
    public $floatFields = ['rate'];

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $countryName = new Zend_Form_Element_Note('country_name');
        $countryName->setLabel('country');

        $rate = new Zend_Form_Element_Note('rate');
        $rate->setLabel('rate');

        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
            ->setLabel('updated_at')
        ;

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy->setLabel('updated_by');

        return [
            $id,
            $countryName,
            $rate,
            $updatedAt,
            $updatedBy,
        ];
    }

}