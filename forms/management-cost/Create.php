<?php

/**
 * Class Lease_Form_ManagementCost_Create
 */
class Lease_Form_ManagementCost_Create extends Lease_Form_BaseCreate
{
    public $mainGroup = 'management_cost';
    public $floatFields = ['rate'];
    public $requiredFields = ['countryCode'];

    /**
     * @var Lease_Service_ManagementCost
     */
    public $managementCostService;

    public function initElements()
    {
        $this->managementCostService = new Lease_Service_ManagementCost();
        parent::initElements();
    }

    protected function getCountries()
    {
        return $this->managementCostService->getCountryOptions();
    }

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $country = new Zend_Form_Element_Select('countryCode');
        $country
            ->setMultiOptions($this->getCountries())
            ->setLabel('country')
        ;

        $rate = new Zend_Form_Element_Text('rate');
        $rate->setLabel('rate');

        return [
            $id,
            $country,
            $rate,
        ];
    }

}