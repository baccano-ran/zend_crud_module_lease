<?php

/**
 * Class Lease_Form_BaseView
 */
class Lease_Form_BaseView extends Lease_Form_Base
{
    public function initElements()
    {
        parent::initElements();
        $this->setNotEmptyElements();
    }

    /**
     * @return array
     */
    protected function _getLastElements()
    {
        $controlButton = new Lease_Form_Element_ControlButton('controlButton');
        $controlButton->setButtons([
            Lease_Form_Element_ControlButton::BUTTON_EDIT,
            Lease_Form_Element_ControlButton::BUTTON_CANCEL,
        ]);

        $closing = new Lease_Form_Element_LastClosing('closing');

        return [
            $controlButton,
            $closing,
        ];
    }
}

