<?php

/**
 * Class Lease_Form_Element_ControlButton
 */
class Lease_Form_Element_ControlButton extends Zend_Form_Element_Hidden
{
    const BUTTON_SAVEANDCANCEL = 1;
    const BUTTON_SAVE = 2;
    const BUTTON_EDIT = 3;
    const BUTTON_CANCEL = 4;
    const BUTTON_SAVEANDEDIT = 5;

    const DEFAULT_VALUE = 'SAVE';
    const JS_CLASS_PREFIX = 'js-button-';

    const WRAPPER_CLASS = 'control-buttons-wrapper';

    protected $_buttonNames = [
        self::BUTTON_SAVEANDCANCEL => 'save_and_close',
        self::BUTTON_SAVEANDEDIT => 'save_and_edit',
        self::BUTTON_SAVE => 'save',
        self::BUTTON_EDIT => 'edit',
        self::BUTTON_CANCEL => 'cancel',
    ];

    protected $_buttons = [
        self::BUTTON_SAVEANDCANCEL,
        self::BUTTON_SAVE,
        self::BUTTON_CANCEL,
    ];

    /**
     * @param Zend_View_Interface|null $view
     * @return string
     */
    public function render(Zend_View_Interface $view = null)
    {
        if (!$this->getValue()) {
            $this->setValue(self::DEFAULT_VALUE);
        }

        /** @var Zend_Form_Decorator_HtmlTag $tagDecorator */
        $tagDecorator = $this->getDecorator('HtmlTag');
        $tagDecorator->setOption('openOnly', true);
        $tag = $tagDecorator->getTag();

        $res = '<div class="' . self::WRAPPER_CLASS . '">' . parent::render($view);
        foreach ($this->_buildAllButtons() as $button) {
            $res .= $button;
        }

        return $res .  '</' . $tag . '></div>';
    }

    /**
     * @param array $buttonTypes
     * @return $this
     */
    public function setButtons(array $buttonTypes = [])
    {
        $this->_buttons = $buttonTypes;
        return $this;
    }

    /**
     * @return Zend_Form_Element_Button[]
     */
    protected function _buildAllButtons()
    {
        $buttons = [];
        foreach ($this->_buttons as $buttonType) {
            $buttons[$buttonType] = $this->_buttonFactory($buttonType);
        }
        return $buttons;
    }

    /**
     * @param int $btnType
     * @return Zend_Form_Element_Button
     */
    public function getButton($btnType)
    {
        return $this->_buttonFactory($btnType);
    }

    /**
     * @param int $btnType
     * @return Zend_Form_Element_Button
     */
    protected $_btnFactoryCache = [];
    protected function _buttonFactory($btnType)
    {
        if (!isset($this->_btnFactoryCache[$btnType])) {
            $btnName = $this->_buttonNames[$btnType];
            $this->_btnFactoryCache[$btnType] = $this->_buildButton($btnName)
                ->setLabel($btnName)
                ->setAttrib('class', static::JS_CLASS_PREFIX . $btnName)
            ;
        }

        return $this->_btnFactoryCache[$btnType];
    }

    /**
     * @param string $name
     * @return Zend_Form_Element_Button
     */
    protected function _buildButton($name)
    {
        $select = new Zend_Form_Element_Button($name);
        $this->_removeDefaultDecorators($select);
        return $select;
    }

    protected function _removeDefaultDecorators(Zend_Form_Element $element)
    {
        $element
            ->removeDecorator('DtDdWrapper')
            ->removeDecorator('HtmlTag')
            ->removeDecorator('Label')
            ->removeDecorator('Errors')
        ;
    }
}