<?php

/**
 * Class Lease_Form_Element_Grid
 *
 * @method string getSourceUrl()
 * @method $this setSourceUrl(string $value)
 *
 * @method string getElementClass()
 * @method $this setElementClass(string $value)
 *
 * @method string getNewItemUrl()
 * @method $this setNewItemUrl(string $value)
 *
 * @method string getTemplate()
 * @method $this setTemplate(string $value)
 *
 * @method bool getEditable()
 * @method $this setEditable(bool $value)
 *
 */
class Lease_Form_Element_Grid extends Zend_Form_Element_Hidden
{
    use BAS_Shared_Lease_TGSPattern;

    protected $_editable = true;
    protected $_sourceUrl;
    protected $_elementClass = 'grid-element';
    protected $_hashObj;
    protected $_newItemUrl;
    protected $_newItemTemplate = '<a href="{new_item_url}"><img src="/vbd/public/images/add.png">Add new item</a>';
    protected $_template = '
        <div {attributes}>{parent}
            <div class="container solid-text">
            {new_item}
            </div>
        </{tag}></div>{script}
    ';

    /**
     * Lease_Form_Element_Grid constructor.
     * @param array|string|Zend_Config $spec
     * @param array|null|Zend_Config $sourceUrl
     * @param array|null $options
     */
    public function __construct($spec, $sourceUrl, $options = null)
    {
        $this->_sourceUrl = $sourceUrl;
        $this->_hashObj = spl_object_hash($this);
        parent::__construct($spec, $options);
    }

    /**
     * @return string
     */
    protected function _urlToGridAjaxFormat()
    {
        return str_replace('/vbd/public/', '', $this->_sourceUrl) . '/gridmod1/ajax/_zfgid/1';
    }

    /**
     * @param Zend_View_Interface|null $view
     * @return string
     */
    public function render(Zend_View_Interface $view = null)
    {
        /** @var Zend_Form_Decorator_HtmlTag $tagDecorator */
        $tagDecorator = $this->getDecorator('HtmlTag');
        $tagDecorator->setOption('openOnly', true);
        $tag = $tagDecorator->getTag();

        $newItem = $this->_newItemUrl === null
            ? ''
            : strtr($this->_newItemTemplate, ['{new_item_url}' => $this->_newItemUrl]);

        $sourceUrl = ' data-url="' . $this->_urlToGridAjaxFormat() . '"';
        $dataHash = ' data-hash ="' . $this->_hashObj . '"';
        $class = ' class="' . $this->_elementClass . '"';

        return strtr($this->_template, [
            '{attributes}' => $class . $dataHash . $sourceUrl,
            '{parent}' => parent::render($view),
            '{new_item}' => $newItem,
            '{tag}' => $tag,
            '{script}' => $this->getScript(),
        ]);
    }

    /**
     * @return string
     */
    public function getScript()
    {
        return <<<HTML
        <script> $(function() {
        var container = $('.$this->_elementClass[data-hash="$this->_hashObj"] .container');
        $.ajax({
            url: '$this->_sourceUrl'
        }).done(function(response) {
            var app = window.APP,
                grid = $(response.grid),
                script = document.createElement("SCRIPT");
                script.text = response.script
            ;

            if (!Boolean("$this->_editable")) {
                grid.find('.add-entity-block').remove();
                grid.find('table .js-edit').remove();
                grid.find('table .js-delete').remove();
            }
            
            grid.append(script);
            container.append(grid);

            if ((app instanceof Object) && (app.hasOwnProperty('eventManager'))) {
                app.eventManager.trigger('gridLoad', grid);
            }
        });
        }); </script>
HTML;
    }

}
