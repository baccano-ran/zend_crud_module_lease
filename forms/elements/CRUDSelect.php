<?php

/**
 * Class Lease_Form_Element_CRUDSelect
 *
 * @method array getActionUrls()
 * @method $this setActionUrls(array $value)
 *
 * @method int getHashObj()
 * @method $this setHashObj(int $value)
 *
 * @method array getButtons()
 * @method $this setButtons(array $value)
 *
 * @method string getElementClass()
 * @method $this setElementClass(string $value)
 *
 * @method string getButtonTemplate()
 * @method $this setButtonTemplate(string $value)
 *
 * @method string getTemplate()
 * @method $this setTemplate(string $value)
 *
 */
class Lease_Form_Element_CRUDSelect extends Zend_Form_Element_Select
{
    use BAS_Shared_Lease_TGSPattern;

    const BUTTON_EDIT = 'edit';
    const BUTTON_ADD = 'add';

    protected $_actionUrls;
    protected $_hashObj;
    protected $_buttons = [
        self::BUTTON_EDIT => [
            'html' => 'edit',
        ],
        self::BUTTON_ADD => [
            'html' => 'add',
        ],
    ];
    protected $_elementClass = 'select-crud-element';
    protected $_buttonTemplate = '<a href="{link}" class="{class}">{content}</a>';
    protected $_template = '
        <div {attributes}>{parent}
            <div class="control">
            {buttons}
            </div>
        </{tag}></div>{script}
    ';

    /**
     * Lease_Form_Element_CRUDSelect constructor.
     * @param array|string|Zend_Config $spec
     * @param array $actionUrls
     * @param null $options
     */
    public function __construct($spec, $actionUrls = [], $options = null)
    {
        $this->_actionUrls = $actionUrls;
        $this->_hashObj = spl_object_hash($this);
        parent::__construct($spec, $options);
    }

    /**
     * @return string
     */
    protected function _buildButtons()
    {
        $res = '';
        foreach ($this->_buttons as $type => $params) {
            $res .= strtr($this->_buttonTemplate, [
                '{link}' => $this->_actionUrls[$type],
                '{class}' => $this->_getBtnClass($type),
                '{content}' => $params['html'],
            ]);
        }
        return $res;
    }

    /**
     * @param string $btnType
     * @return string
     */
    protected function _getBtnClass($btnType)
    {
        return 'js-' . $btnType;
    }

    /**
     * @param Zend_View_Interface|null $view
     * @return string
     */
    public function render(Zend_View_Interface $view = null)
    {
        /** @var Zend_Form_Decorator_HtmlTag $tagDecorator */
        $tagDecorator = $this->getDecorator('HtmlTag');
        $tagDecorator->setOption('openOnly', true);
        $tag = $tagDecorator->getTag();

        self::BUTTON_ADD;
        return strtr($this->_template, [
            '{attributes}' => ' data-hash ="' . $this->_hashObj . '" class="' . $this->_elementClass . '"',
            '{parent}' => parent::render($view),
            '{buttons}' => $this->_buildButtons(),
            '{tag}' => $tag,
            '{script}' => $this->getScript(),
        ]);
    }

    /**
     * @return string
     */
    public function getScript()
    {
        $editBtnClass = $this->_getBtnClass(self::BUTTON_EDIT);
        return <<<HTML
        <script>
        $(function() {
            var element = $('.$this->_elementClass[data-hash="$this->_hashObj"]'),
                editButton = element.find('.{$editBtnClass}'),
                selectBox = element.find('select')
            ;
            
            editButton.on('click', function(e) {
                e.preventDefault();
                if (selectBox.val() === '') return false;
                window.location.href = this.href + '/id/' + selectBox.val();
            });
        });
        </script>
HTML;

    }

}

