<?php

/**
 * Class Lease_Form_VatRate_Create
 */
class Lease_Form_VatRate_Create extends Lease_Form_BaseCreate
{
    public $mainGroup = 'vat_rate';

    public $requiredFields = [
        'standardRate',
    ];

    public $floatFields = [
        'standardRate',
        'reducedRate',
    ];

    /**
     * @return array
     */
    public function getCountryOptions()
    {
        $service = new Lease_Service_VatRate();
        return ['' => 'none'] + $service->getCountryOptions();
    }

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');
        
        $countryCode = new Zend_Form_Element_Select('countryCode');
        $countryCode
            ->addMultiOptions($this->getCountryOptions())
            ->setLabel('label_country')
        ;

        $standardRate = new Zend_Form_Element_Text('standardRate');
        $standardRate
            ->setLabel('standard_rate')
        ;

        $reducedRate = new Zend_Form_Element_Text('reducedRate');
        $reducedRate
            ->setLabel('reduced_rate')
        ;

        return [
            $id,
            $countryCode,
            $standardRate,
            $reducedRate,
        ];
    }

}