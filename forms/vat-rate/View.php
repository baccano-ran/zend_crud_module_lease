<?php

/**
 * Class Lease_Form_VatRate_View
 */
class Lease_Form_VatRate_View extends Lease_Form_BaseView
{
    public $floatFields = [
        'standardRate',
        'reducedRate',
    ];

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $countryCode = new Zend_Form_Element_Note('country_name');
        $countryCode
            ->setLabel('label_country')
        ;

        $standardRate = new Zend_Form_Element_Note('standardRate');
        $standardRate
            ->setLabel('standard_rate')
        ;

        $reducedRate = new Zend_Form_Element_Note('reducedRate');
        $reducedRate
            ->setLabel('reduced_rate')
        ;

        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt
            ->setLabel('updated_at')
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
        ;

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy
            ->setLabel('updated_by')
        ;

        return [
            $id,
            $countryCode,
            $standardRate,
            $reducedRate,
            $updatedAt,
            $updatedBy,
        ];
    }

}