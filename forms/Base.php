<?php

/**
 * Class Lease_Form_Base
 * @method Zend_View getView()
 */
class Lease_Form_Base extends Zend_Form
{
    public $mainGroup = 'lease';

    public $floatFields = [ ];
    public $requiredFields = [ ];
    public $hiddenFields = ['id'];
    public $defaultValues = [ ];

    const FIELD_INT = 'typeInt';
    const FIELD_FLOAT = 'typeFloat';
    const FIELD_STRING = 'typeString';

    const FIELD_REQUIRE = 'require';
    const FIELD_HIDDEN = 'hidden';

    public $fields = [];

    public function init()
    {
        parent::init();

        $this->addPrefixPath('Lease_Form_Element',
            APPLICATION_PATH . '/vbd/application/modules/lease/forms/elements', Zend_Form::ELEMENT);
    }

    /**
     * @return Lease_Library_FormElementBuilder
     */
    public function getElementBuilder()
    {
        return new Lease_Library_FormElementBuilder($this);
    }

    /**
     * @param Zend_Form_Element $field
     * @param $type
     * @throws Zend_Form_Exception
     */
    protected function _applyFieldType(Zend_Form_Element $field, $type)
    {
        switch ($type) {
            case self::FIELD_INT:
                $field->addValidator(new Zend_Validate_Int());
                break;
            case self::FIELD_FLOAT:
                $field->addValidator(new Zend_Validate_Float());
                break;

        }
    }

    public function applyFieldOptions()
    {
        /** @var Zend_Form_Element $element */
        foreach ($this->getElements() as $element) {
            $fName = $element->getName();
            if (array_key_exists($fName, $this->fields)) {

                foreach ($this->fields[$fName] as $key => $option) {
                    if (is_numeric($key)) {
                        $this->_applyFieldType($element, $option);
                    }
                }

            }
        }
    }

    /**
     * @return BAS_Shared_Model_Lease_AbstractModel
     */
    public function getEntity() {}

    protected function _getElements() { return []; }
    protected function _getLastElements() { return []; }

    /**
     * @return string
     */
    protected function _getCurrentDecimalSeparator()
    {
        return Lease_Service_BaseService::getDecimalSeparator();
    }

    /**
     * @return mixed
     */
    protected function _getCurrentLocale()
    {
        return Lease_Service_BaseService::getCurrentLocale();
    }

    /**
     * @throws Zend_Form_Exception
     */
    protected function _initFloatFields()
    {
        if (empty($this->floatFields)) return;

        $floatFilter = new Lease_Form_Filter_Float($this->_getCurrentDecimalSeparator());
        $floatValidator = new Zend_Validate_Float($this->_getCurrentLocale());

        foreach ($this->floatFields as $field) {
            if ($element = $this->getElement($field)) {
                $element
                    ->addFilter($floatFilter)
                    ->addValidator($floatValidator)
                ;
            }
        }
    }

    protected function _initHiddenFields()
    {
        if (empty($this->hiddenFields)) return;

        foreach ($this->hiddenFields as $field) {
            if ($element = $this->getElement($field)) {
                $element
                    ->removeDecorator('DtDdWrapper')
                    ->removeDecorator('Label')
                    ->getDecorator('HtmlTag')->setOption('class', 'form-hidden_field')
                ;
            }
        }
    }

    protected function _initRequiredFields()
    {
        if (empty($this->requiredFields)) return;

        foreach ($this->requiredFields as $field) {
            if ($element = $this->getElement($field)) {
                $element
                    ->setRequired()
                    ->getDecorator('Label')->setOption('class', 'form-element-label form-required_field')
                ;
            }
        }
    }

    protected function _initDefaultValues()
    {
        if (empty($this->defaultValues)) return;
        $this->populate($this->defaultValues);
    }

    public function initElements()
    {
        $elements = array_merge($this->_getElements(), $this->_getLastElements());
        /** @var Zend_Form_Element $element */
        foreach ($elements as $element) {
            $element->setBelongsTo($this->mainGroup);
        }

        $this->addElements($elements);
        $this->_initHiddenFields();
        $this->_initFloatFields();
        $this->_initDefaultValues();
        $this->_initRequiredFields();
        $this->addAttribs($this->getDataAttrActionUrls());
    }
    
    public function setNotEmptyElements()
    {
        $elements = $this->getElements();
        $notEmptyFilter = new Lease_Form_Filter_NotEmpty();

        /** @var Zend_Form_Element $element */
        foreach ($elements as $element) {
            $element->addFilter($notEmptyFilter);
        }
    }

    /**
     * @param bool|false $suppressArrayNotation
     * @return null
     */
    public function getValues($suppressArrayNotation = false)
    {
        $values = $this->emptyStrToNull(parent::getValues($suppressArrayNotation));
        $values = $values[$this->mainGroup];

        foreach ($this->floatFields as $field) {
            $values[$field] = $this->_floatToDB($values[$field]);
        }

        return $values;
    }

    /**
     * @param array $data
     * @return null
     */
    public function emptyStrToNull($data)
    {
        if (is_array($data)) {
            foreach ($data as &$item) {
                if (is_array($item)) {
                    $item = $this->emptyStrToNull($item);
                } elseif ($item === '') {
                    $item = null;
                }
            }
        } elseif ($data === '') {
            $data = null;
        }

        return $data;
    }

    /**
     * @param $value
     * @return mixed
     */
    protected function _floatToDB($value)
    {
        return $value ? str_replace(',', '.', $value) : $value;
    }

    /**
     * @param Zend_Form_Element[] $elements
     * @param array $assignDecorators
     * @return Zend_Form_Element[]
     */
    protected function _removeDefaultDecorators(array $elements, array $assignDecorators = ['ViewHelper'])
    {
        array_walk($elements, function (Zend_Form_Element $element) use ($assignDecorators) {
            $element->setDecorators($assignDecorators);
        });

        return $elements;
    }

    /**
     * @return array
     */
    public function getDataAttrActionUrls()
    {
        $actionUrls = $this->getAttrib('actionUrls');

        $dataAttr = [];
        foreach ($actionUrls as $name => $url) {
            $dataAttr['data-url-' . $name] = $url;
        }

        return $dataAttr;
    }
}