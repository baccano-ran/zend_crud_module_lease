<?php

/**
 * Class Lease_Form_BaseCreate
 */
class Lease_Form_BaseCreate extends Lease_Form_Base
{
    /**
     * @return array
     */
    protected function _getLastElements()
    {
        $controlButton = new Lease_Form_Element_ControlButton('controlButton');
        $controlButton->setButtons([
            Lease_Form_Element_ControlButton::BUTTON_SAVEANDCANCEL,
            Lease_Form_Element_ControlButton::BUTTON_SAVEANDEDIT,
            Lease_Form_Element_ControlButton::BUTTON_CANCEL,
        ]);

        $closing = new Lease_Form_Element_LastClosing('closing');

        return [
            $controlButton,
            $closing,
        ];
    }
}

