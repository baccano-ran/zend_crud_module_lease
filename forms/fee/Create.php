<?php

/**
 * Class Lease_Form_Fee_Create
 */
class Lease_Form_Fee_Create extends Lease_Form_BaseCreate
{
    public $mainGroup = 'fee';
    public $floatFields = ['amount'];
    public $requiredFields = ['leaseFeeCategoryId', 'countryCode'];

    /** @var Lease_Service_Fee */
    public $feeService;

    public function initElements()
    {
        $this->feeService = new Lease_Service_Fee();
        parent::initElements();
    }

    /**
     * @return string[]
     */
    protected function _getCategoryOptions()
    {
        $confService = new Lease_Service_Fee();
        return ['' => 'none'] + $confService->getCategoryOptions();
    }

    /**
     * @return string[]
     */
    protected function _getCountryOptions()
    {
        $confService = new Lease_Service_Configuration();
        return ['' => 'none'] + $confService->getActiveConfCountryOptions();
    }

    /**
     * @return string[]
     * @throws BAS_Shared_NotFoundException
     */
    protected function _getVehicleConditions()
    {
        $interestService = new Lease_Service_Interest();
        return ['' => 'none'] + $interestService->getVehicleConditionOptions();
    }

    /**
     * @return Zend_Form_Element[]
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');
        
        $leaseFeeCategoryId = new Lease_Form_Element_CRUDSelect('leaseFeeCategoryId');
        $leaseFeeCategoryId
            ->setActionUrls($this->getAttrib('feeCategoryActionUrls'))
            ->setMultiOptions($this->_getCategoryOptions())
            ->setLabel('category')
        ;

        $countryCode = new Zend_Form_Element_Select('countryCode');
        $countryCode
            ->setMultiOptions($this->_getCountryOptions())
            ->setLabel('country')
        ;

        $amount = new Zend_Form_Element_Text('amount');
        $amount
            ->setLabel('amount')
        ;

        $vehicleCondition = new Zend_Form_Element_Select('vehicleCondition');
        $vehicleCondition
            ->setMultiOptions($this->_getVehicleConditions())
            ->setLabel('vehicle_condition')
        ;

        $active = new Zend_Form_Element_Checkbox('active');
        $active
            ->setLabel('active')
        ;

        return [
            $id,
            $leaseFeeCategoryId,
            $countryCode,
            $amount,
            $vehicleCondition,
            $active,
        ];
    }

}