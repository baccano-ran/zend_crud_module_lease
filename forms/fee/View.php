<?php

/**
 * Class Lease_Form_Fee_View
 */
class Lease_Form_Fee_View extends Lease_Form_BaseView
{
    public $floatFields = ['amount'];

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $category = new Zend_Form_Element_Note('fee_category');
        $category
            ->setLabel('category')
        ;

        $country = new Zend_Form_Element_Note('country_name');
        $country
            ->setLabel('country')
        ;

        $amount = new Zend_Form_Element_Note('amount');
        $amount
            ->addFilter(new Lease_Form_Filter_Template(['before' => '&euro;']))
            ->setLabel('amount')
        ;

        $condition = new Zend_Form_Element_Note('vehicleCondition');
        $condition
            ->addFilter(new Lease_Form_Filter_VehicleCondition())
            ->setLabel('condition')
        ;

        $active = new Zend_Form_Element_Note('active');
        $active
            ->addFilter(new Lease_Form_Filter_YesNo())
            ->setLabel('active')
        ;

        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
            ->setLabel('updated_at')
        ;

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy->setLabel('updated_by');

        return [
            $id,
            $category,
            $country,
            $amount,
            $condition,
            $active,
            $updatedAt,
            $updatedBy,
        ];
    }

}