<?php

/**
 * Class Lease_Form_Filter_VehicleCondition
 */
class Lease_Form_Filter_VehicleCondition extends Zend_Filter
{
    /**
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        if (class_exists($value)) {
            return $value::describe();
        }

        return $value;
    }
}

