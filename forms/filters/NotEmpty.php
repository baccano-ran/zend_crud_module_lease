<?php

/**
 * Class Pricing_Filter_NotEmpty
 */
class Lease_Form_Filter_NotEmpty implements Zend_Filter_Interface
{
    /**
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        return
            ($value === '') ||
            ($value === ' ') ||
            ($value === null) ||
            ($value === false)
                ? '&nbsp;' : $value;
    }
}
