<?php

/**
 * Class Lease_Form_Filter_DateFormat
 */
class Lease_Form_Filter_DateFormat extends Zend_Filter
{
    public $format = 'd F Y';

    public function __construct($format = null)
    {
        if ($format !== null) {
            $this->format = $format;
        }
    }

    /**
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        return $value ? strtolower(date($this->format, strtotime($value))) : $value;
    }
}