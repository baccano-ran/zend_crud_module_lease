<?php

/**
 * Class Lease_Form_Filter_Float
 */
class Lease_Form_Filter_Float extends Zend_Filter
{
    private $_separator;
    private $_replaceMap = [
        ',' => '.',
        '.' => ',',
    ];

    /**
     * Lease_Form_Filter_FloatField constructor.
     * @param string $separator
     */
    public function __construct($separator = '.')
    {
        $this->_separator = $separator;
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function filter($value)
    {
        return str_replace(
            $this->_replaceMap[$this->_separator],
            $this->_separator,
            $value
        );
    }
}