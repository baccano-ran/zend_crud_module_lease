<?php

/**
 * Class Lease_Form_Filter_YesNo
 */
class Lease_Form_Filter_YesNo extends Zend_Filter
{
    /**
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        return $value ? 'yes' : 'no';
    }
}