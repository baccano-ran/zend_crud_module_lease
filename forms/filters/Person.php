<?php

/**
 * Class Lease_Form_Filter_Person
 */
class Lease_Form_Filter_Person extends Zend_Filter
{
    public $abbreviation;

    /**
     * Lease_Form_Filter_PersonField constructor.
     * @param string $abbreviation
     */
    public function __construct($abbreviation = null)
    {
        $this->abbreviation = $abbreviation;
    }

    /**
     * @param mixed $value
     * @param string|null $abbreviation
     * @return string
     */
    public function filter($value, $abbreviation = null)
    {
        if ($abbreviation === null) {
            $abbreviation = $this->abbreviation;
        }
        return $value . ' (' . $abbreviation . ')';
    }
}