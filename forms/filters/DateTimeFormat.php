<?php

/**
 * Class Lease_Form_Filter_DateTimeFormat
 */
class Lease_Form_Filter_DateTimeFormat extends Lease_Form_Filter_DateFormat
{
    public $format = 'd F Y H:i';
}