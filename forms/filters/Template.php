<?php

/**
 * Class Lease_Form_Filter_Template
 *
 * @method string getBefore()
 * @method $this setBefore(string $value)
 *
 * @method string getAfter()
 * @method $this setAfter(string $value)
 *
 * @method string getTemplate()
 * @method $this setTemplate(string $value)
 *
 * @method bool getCheckEmpty()
 * @method $this setCheckEmpty(bool $value)
 *
 */
class Lease_Form_Filter_Template extends Zend_Filter
{
    use BAS_Shared_Lease_TGSPattern;

    protected $_template = '{before}{value}{after}';
    protected $_before = '';
    protected $_after = '';
    protected $_checkEmpty = true;

    public function __construct(array $parts = [])
    {
        foreach ($parts as $key => $value) {
            $setter = 'set' . ucfirst($key);
            $this->$setter($value);
        }
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function filter($value)
    {
        return ($this->getCheckEmpty() && (($value === null) || ($value === '')))
            ? $value
            : strtr($this->getTemplate(), [
                '{value}' => $value,
                '{before}' => $this->getBefore(),
                '{after}' => $this->getAfter(),
            ]);
    }
}
