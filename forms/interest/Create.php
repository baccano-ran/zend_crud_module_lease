<?php

/**
 * Class Lease_Form_Interest_Create
 */
class Lease_Form_Interest_Create extends Lease_Form_BaseCreate
{
    public $mainGroup = 'interest';
    public $floatFields = ['markup'];
    public $requiredFields = [
        'countryCode',
        'leaseIborId',
        'markup',
    ];

    /**
     * @var Lease_Service_Interest
     */
    public $interestService;

    public function initElements()
    {
        $this->interestService = new Lease_Service_Interest();
        parent::initElements();
    }

    protected function _getCountries()
    {
        return ['' => 'none'] + $this->interestService->getCountryOptions();
    }

    protected function _getVehicleConditions()
    {
        return ['' => 'none'] + $this->interestService->getVehicleConditionOptions();
    }

    protected function _getIborOptions()
    {
        return ['' => 'none'] + $this->interestService->getIborOptions();
    }

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $country = new Zend_Form_Element_Select('countryCode');
        $country
            ->setMultiOptions($this->_getCountries())
            ->setLabel('label_country')
        ;

        $vehicleCondition = new Zend_Form_Element_Select('vehicleCondition');
        $vehicleCondition
            ->setMultiOptions($this->_getVehicleConditions())
            ->setLabel('vehicle_condition')
        ;

        $ibor = new Zend_Form_Element_Select('leaseIborId');
        $ibor
            ->setMultiOptions($this->_getIborOptions())
            ->setLabel('ibor')
        ;

        $markup = new Zend_Form_Element_Text('markup');
        $markup->setLabel('markup');

        return [
            $id,
            $country,
            $vehicleCondition,
            $ibor,
            $markup,
        ];
    }

}