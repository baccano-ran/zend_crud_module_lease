<?php

/**
 * Class Lease_Form_Interest_View
 */
class Lease_Form_Interest_View extends Lease_Form_BaseView
{
    public $floatFields = ['markup'];

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $leaseIborId = new Zend_Form_Element_Note('lirInfo');
        $leaseIborId->setLabel('ibor');

        $countryName = new Zend_Form_Element_Note('country_name');
        $countryName->setLabel('country');

        $markup = new Zend_Form_Element_Note('markup');
        $markup->setLabel('markup');

        $vehicleCondition = new Zend_Form_Element_Note('vehicleCondition');
        $vehicleCondition
            ->addFilter(new Lease_Form_Filter_VehicleCondition())
            ->setLabel('vehicle_condition')
        ;

        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
            ->setLabel('updated_at')
        ;

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy->setLabel('updated_by');

        return [
            $id,
            $countryName,
            $vehicleCondition,
            $leaseIborId,
            $markup,
            $updatedAt,
            $updatedBy,
        ];
    }

}