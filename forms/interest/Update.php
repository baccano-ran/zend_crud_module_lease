<?php

/**
 * Class Lease_Form_Interest_Update
 */
class Lease_Form_Interest_Update extends Lease_Form_Interest_Create
{
    protected function _getElements()
    {
        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt->addFilter(new Lease_Form_Filter_DateTimeFormat());
        $updatedAt->setLabel('updated_at');

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy->setLabel('updated_by');

        return array_merge(parent::_getElements(), [
            $updatedAt,
            $updatedBy,
        ]);
    }
}
