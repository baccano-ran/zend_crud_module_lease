<?php
/**
 * Zend form: Option max downpayment create form.
 * 
 * All form fields option max down payment create.
 * 
 * @package Form
 * @extends Lease_Form_BaseCreate
 */
class Lease_Form_OptionMaxDownpayment_Create extends Lease_Form_BaseCreate
{
    /** @var string $mainGroup */
    public $mainGroup = 'option_max_downpayment';

    /** @var Lease_Service_OptionMaxDownpayment $optionMaxDownpaymentService */
    public $optionMaxDownpaymentService;

    /** @var array $hiddenFields */
    public $hiddenFields = [
        'id',
        'leaseOptionsId'
    ];

    /** @var array $floatFields */
    public $floatFields = [
        'maximumDownpaymentRate',
    ];

    /**
     * Element initialization.
     */
    public function initElements()
    {
        $this->optionMaxDownpaymentService = new Lease_Service_OptionMaxDownpayment();
        parent::initElements();
    }

    /**
     * Get form elements.
     *
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');
        
        $leaseOptionsId = clone $id;
        $leaseOptionsId->setName('leaseOptionsId');
        
        if ($optionId = $this->getAttrib('leaseOptionsId')) {
            $leaseOptionsId->setValue($optionId);
        }

        $salesPriceMin = new Zend_Form_Element_Text('salesPriceMin');
        $salesPriceMin->setLabel('sales_price_min');

        $salesPriceMax = clone $salesPriceMin;
        $salesPriceMax->setName('salesPriceMax')
            ->setLabel('sales_price_max');
        
        $maximumDownpaymentRate = clone $salesPriceMin;
        $maximumDownpaymentRate->setName('maximumDownpaymentRate')
            ->setLabel('maximum_downpayment_rate');
        
        return [
            $id,
            $leaseOptionsId,
            $salesPriceMin,
            $salesPriceMax,
            $maximumDownpaymentRate,
        ];
    }
}