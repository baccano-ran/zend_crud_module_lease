<?php
/**
 * Zend form: Option max downpayment update form.
 * 
 * All form fields option max down payment update.
 * 
 * @package Form
 * @extends Lease_Form_OptionMaxDownpayment_Create
 */
class Lease_Form_OptionMaxDownpayment_Update extends Lease_Form_OptionMaxDownpayment_Create
{
    /**
     * Get form elements.
     *
     * @return array
     */
    protected function _getElements()
    {
        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt->addFilter(new Lease_Form_Filter_DateTimeFormat());
        $updatedAt->setLabel('updated_at');

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy->setLabel('updated_by');

        return array_merge(parent::_getElements(), [
            $updatedAt,
            $updatedBy,
        ]);
    }
}
