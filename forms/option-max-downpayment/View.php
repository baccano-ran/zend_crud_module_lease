<?php
/**
 * Zend form: Option max downpayment view form.
 * 
 * All form fields option max down payment view.
 * 
 * @package Form
 * @extends Lease_Form_BaseView
 */
class Lease_Form_OptionMaxDownpayment_View extends Lease_Form_BaseView
{
    /** @var array $floatFields */
    public $floatFields = [
        'maximumDownpaymentRate',
    ];

    /** @var array $hiddenFields */
    public $hiddenFields = [
        'id',
        'leaseOptionsId',
    ];

    /**
     * Get form elements.
     *
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');
        $leaseOptionsId = clone $id;
        $leaseOptionsId->setName('leaseOptionsId');
        
        $tenor = new Zend_Form_Element_Note('tenor');
        $tenor->setLabel('tenor') ;

        $salesPriceMin = new Zend_Form_Element_Note('salesPriceMins');
        $salesPriceMin->setLabel('sales_price_min');

        $salesPriceMax = new Zend_Form_Element_Note('salesPriceMax');
        $salesPriceMax->setLabel('sales_price_max');

        $maximumDownpaymentRate = new Zend_Form_Element_Note('maximumDownpaymentRate');
        $maximumDownpaymentRate->setLabel('maximum_downpayment_rate');

        return [
            $id,
            $leaseOptionsId,
            $salesPriceMin,
            $salesPriceMax,
            $maximumDownpaymentRate,
        ];
    }
}