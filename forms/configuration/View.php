<?php

/**
 * Class Lease_Form_Configuration_View
 */
class Lease_Form_Configuration_View extends Lease_Form_BaseView
{
    /**
     * @return array
     */
    protected function _getElements()
    {
        $builder = $this->getElementBuilder();

        $builder
            ->createElement('note', 'id', [], ['hidden'])
            ->createElement('note', 'country_name', ['label' => 'country'])
            ->createElement('note', 'calculateInPrenumerando', ['label' => 'calculate_in_prenumerando'], ['bool'])
            ->createElement('note', 'calculateWithResidualValue', ['label' => 'calculate_with_residual_value'], ['bool'])
            ->createElement('note', 'currency', ['label' => 'currency'])
            ->createElement('note', 'contactFormRecipients', ['label' => 'contact_form_recipients'])
            ->createElement('note', 'applyFormRecipients', ['label' => 'apply_form_recipients'])
            ->createElement('note', 'inquiryRegisterTicket', ['label' => 'inquiry_register_ticket'], ['bool'])
            ->createElement('note', 'ticketSourceNameContact', ['label' => 'ticket_source_name_contact'])
            ->createElement('note', 'ticketSourceNameApply', ['label' => 'ticket_source_name_apply'])
            ->createElement('note', 'minimumLeasePrice', ['label' => 'minimum_lease_price'], ['float'])
            ->createElement('note', 'minimumSalesPrice', ['label' => 'minimum_sales_price'], ['float'])
            ->createElement('note', 'maxAgeAfterLease', ['label' => 'max_age_after_lease'], ['float'])
            ->createElement('note', 'maxAgeStartLease', ['label' => 'max_age_start_lease'], ['float'])
            ->createElement('note', 'updatedAt', ['label' => 'updated_at'], ['DateTime'])
            ->createElement('note', 'updater_full_name', ['label' => 'updated_by'])
            ->createElement('note', 'active', ['label' => 'active'], ['bool'])
        ;

        $options = new Lease_Form_Element_Grid(
            'options',
            $this->getAttrib('optionGridUrl')
        );
        $options
            ->setNewItemUrl($this->getAttrib('newItemUrl'))
            ->setEditable(false)
            ->setLabel('options')
        ;

        return array_merge($builder->getElements(), [$options]);
    }

}