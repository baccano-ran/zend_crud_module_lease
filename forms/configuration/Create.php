<?php

/**
 * Class Lease_Form_Configuration_Create
 */
class Lease_Form_Configuration_Create extends Lease_Form_BaseCreate
{
    public $mainGroup = 'configuration';

    /** @var Lease_Service_Configuration */
    public $configurationService;

    public function initElements()
    {
        $this->configurationService = new Lease_Service_Configuration();
        parent::initElements();
    }

    /**
     * @return array
     */
    public function getCountryOptions()
    {
        return ['' => 'none'] + $this->configurationService->getCountryOptions();
    }

    public function getCurrencyOptions()
    {
        return ['' => 'none'] + BAS_Shared_Model_Lease_Configuration::$currencyList;
    }

    /**
     * @return array|Zend_Form_Element[]
     * @throws Zend_Form_Exception
     */
    protected function _getElements()
    {
        return $this->getElementBuilder()

            ->createElement('hidden', 'id', [], ['hidden'])
            ->createElement('select', 'countryCode', [
                'label' => 'country',
                'multiOptions' => $this->getCountryOptions(),
            ])
            ->createElement('checkbox', 'calculateInPrenumerando', ['label' => 'calculate_in_prenumerando'])
            ->createElement('checkbox', 'calculateWithResidualValue', ['label' => 'calculate_with_residual_value'])
            ->createElement('select', 'currency', [
                'label' => 'currency',
                'multiOptions' => $this->getCurrencyOptions(),
            ])
            ->createElement('text', 'contactFormRecipients', ['label' => 'contact_form_recipients'])
            ->createElement('text', 'applyFormRecipients', ['label' => 'apply_form_recipients'])
            ->createElement('checkbox', 'inquiryRegisterTicket', [
                'label' => 'inquiry_register_ticket', 'value' => true
            ])
            ->createElement('text', 'ticketSourceNameContact', ['label' => 'ticket_source_name_contact'])
            ->createElement('text', 'ticketSourceNameApply', ['label' => 'ticket_source_name_apply'])
            ->createElement('text', 'minimumLeasePrice', [
                'label' => 'minimum_lease_price', 'value' => '130,00',
            ], ['float'])
            ->createElement('text', 'minimumSalesPrice', ['label' => 'minimum_sales_price'], ['float'])
            ->createElement('text', 'maxAgeAfterLease', ['label' => 'max_age_after_lease'], ['float'])
            ->createElement('text', 'maxAgeStartLease', [
                'label' => 'max_age_start_lease', 'value' => '8,00',
            ], ['float'])
            ->createElement('checkbox', 'active', ['label' => 'active'])

            ->getElements()
        ;
    }
}