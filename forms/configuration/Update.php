<?php

/**
 * Class Lease_Form_Configuration_Update
 */
class Lease_Form_Configuration_Update extends Lease_Form_Configuration_Create
{
    protected function _getElements()
    {
        $optionField = new Lease_Form_Element_Grid(
            'options',
            $this->getAttrib('optionGridUrl')
        );
        $optionField
            ->setNewItemUrl($this->getAttrib('newItemUrl'))
            ->setLabel('lease_options')
        ;

        $builder = $this->getElementBuilder()
            ->addElement($optionField)
            ->createElement('note', 'updatedAt', ['label' => 'updated_at'], ['DateTime'])
            ->createElement('note', 'updater_full_name', ['label' => 'updated_by'])
        ;

        return array_merge(parent::_getElements(), $builder->getElements());
    }
}
