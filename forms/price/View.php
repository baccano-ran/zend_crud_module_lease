<?php

/**
 * Class Lease_Form_Price_View
 */
class Lease_Form_Price_View extends Lease_Form_BaseView
{
    public $floatFields = [
        'annuity',
        'residual',
        'downpayment',
        'originalAnnuity',
        'originalResidual',
        'originalDownpayment',
    ];

    public function initElements()
    {
        parent::initElements();
        $this->setNotEmptyElements();
    }

    /**
     * @return array
     */
    protected function _getElements()
    {
        $legacyVoertuigID = new Zend_Form_Element_Note('legacyVoertuigID');
        $legacyVoertuigID->setLabel('legacy_voertuig_id');

        $countryCode = new Zend_Form_Element_Note('country_name');
        $countryCode->setLabel('country');

        $tenor = new Zend_Form_Element_Note('tenor');
        $tenor->setLabel('tenor');

        $legacyTruckID = new Zend_Form_Element_Note('legacyTruckID');
        $legacyTruckID->setLabel('legacy_truck_id');

        $annuity = new Zend_Form_Element_Note('annuity');
        $annuity->setLabel('annuity');

        $residual = new Zend_Form_Element_Note('residual');
        $residual->setLabel('residual');

        $downpayment = new Zend_Form_Element_Note('downpayment');
        $downpayment->setLabel('downpayment');

        $currency = new Zend_Form_Element_Note('currency');
        $currency->setLabel('currency');

        $originalAnnuity = new Zend_Form_Element_Note('originalAnnuity');
        $originalAnnuity->setLabel('original_annuity');

        $originalResidual = new Zend_Form_Element_Note('originalResidual');
        $originalResidual->setLabel('original_residual');

        $originalDownpayment = new Zend_Form_Element_Note('originalDownpayment');
        $originalDownpayment->setLabel('original_downpayment');

        $recalculatedOn = new Zend_Form_Element_Note('recalculatedOn');
        $recalculatedOn
            ->setLabel('recalculated_on')
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
        ;

        $matchedOptionName = new Zend_Form_Element_Note('matchedOptionName');
        $matchedOptionName->setLabel('matched_option_name');

        return [
            $legacyVoertuigID,
            $countryCode,
            $tenor,
            $legacyTruckID,
            $annuity,
            $residual,
            $downpayment,
            $currency,
            $originalAnnuity,
            $originalResidual,
            $originalDownpayment,
            $recalculatedOn,
            $matchedOptionName,
        ];
    }

}