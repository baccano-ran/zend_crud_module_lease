<?php

/**
 * Class Lease_Form_Price_Create
 */
class Lease_Form_Price_Create extends Lease_Form_BaseCreate
{
    public $mainGroup = 'price';

    /** @var Lease_Service_Price */
    public $service;
    public $requiredFields = [
        'legacyVoertuigID',
        'countryCode',
        'tenor',
        'legacyTruckID',
        'annuity',
        'residual',
        'downpayment',
        'currency',
        'originalAnnuity',
        'originalResidual',
        'originalDownpayment',
    ];

    public $floatFields = [
        'annuity',
        'residual',
        'downpayment',
        'originalAnnuity',
        'originalResidual',
        'originalDownpayment',
    ];

    public function initElements()
    {
        $this->service = new Lease_Service_Price();
        parent::initElements();
    }
    
    /**
     * @return array
     */
    public function getCountryOptions()
    {
        return ['' => 'none'] + $this->service->getCountryOptions();
    }
    
    /**
     * @return array
     */
    protected function _getElements()
    {
        $legacyVoertuigID = new Zend_Form_Element_Text('legacyVoertuigID');
        $legacyVoertuigID->setLabel('legacy_voertuig_id');

        $countryCode = new Zend_Form_Element_Select('countryCode');
        $countryCode
            ->setMultiOptions($this->getCountryOptions())
            ->setLabel('country')
        ;

        $tenor = new Zend_Form_Element_Text('tenor');
        $tenor->setLabel('tenor');

        $legacyTruckID = new Zend_Form_Element_Text('legacyTruckID');
        $legacyTruckID->setLabel('legacy_truck_id');

        $annuity = new Zend_Form_Element_Text('annuity');
        $annuity->setLabel('annuity');

        $residual = new Zend_Form_Element_Text('residual');
        $residual->setLabel('residual');

        $downpayment = new Zend_Form_Element_Text('downpayment');
        $downpayment->setLabel('downpayment');

        $currency = new Zend_Form_Element_Text('currency');
        $currency->setLabel('currency');

        $originalAnnuity = new Zend_Form_Element_Text('originalAnnuity');
        $originalAnnuity->setLabel('original_annuity');

        $originalResidual = new Zend_Form_Element_Text('originalResidual');
        $originalResidual->setLabel('original_residual');

        $originalDownpayment = new Zend_Form_Element_Text('originalDownpayment');
        $originalDownpayment->setLabel('original_downpayment');

        $recalculatedOn = new Lease_Form_Element_DateTimeSelect('recalculatedOn');
        $recalculatedOn->setLabel('recalculated_on');

        $matchedOptionName = new Zend_Form_Element_Text('matchedOptionName');
        $matchedOptionName->setLabel('matched_option_name');

        return [
            $legacyVoertuigID,
            $countryCode,
            $tenor,
            $legacyTruckID,
            $annuity,
            $residual,
            $downpayment,
            $currency,
            $originalAnnuity,
            $originalResidual,
            $originalDownpayment,
            $recalculatedOn,
            $matchedOptionName,
        ];
    }
}