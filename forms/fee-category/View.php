<?php

/**
 * Class Lease_Form_FeeCategory_View
 */
class Lease_Form_FeeCategory_View extends Lease_Form_BaseView
{

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $name = new Zend_Form_Element_Note('name');
        $name->setLabel('name');

        return [
            $id,
            $name,
        ];
    }

}