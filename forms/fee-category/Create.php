<?php

/**
 * Class Lease_Form_FeeCategory_Create
 */
class Lease_Form_FeeCategory_Create extends Lease_Form_BaseCreate
{
    public $mainGroup = 'fee_category';
    public $hiddenFields = ['id'];
    public $requiredFields = ['name'];

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $name = new Zend_Form_Element_Text('name');
        $name->setLabel('name');

        return [
            $id,
            $name,
        ];
    }

}