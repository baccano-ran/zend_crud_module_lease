<?php

/**
 * Class Lease_Form_OptionTenor_Create
 */
class Lease_Form_OptionTenor_Create extends Lease_Form_BaseCreate
{
    public $mainGroup = 'option_tenor';

    /** @var Lease_Service_OptionTenor */
    public $optionTenorService;
    public $hiddenFields = ['id', 'leaseOptionsId'];

    public $floatFields = [
        'downpaymentRate',
        'minimalDownpayment',
        'residualValueRate',
        'residualValueFixed',
        'residualValueRateMax',
    ];

    public function initElements()
    {
        $this->optionTenorService = new Lease_Service_OptionTenor();
        parent::initElements();
    }

    protected function _getTenorOptions()
    {
        return ['' => 'none'] + BAS_Shared_Model_Lease_OptionTenor::getTenorOptions();
    }

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $leaseOptionsId = new Zend_Form_Element_Hidden('leaseOptionsId');
        if ($optionsId = $this->getAttrib('leaseOptionsId')) {
            $leaseOptionsId->setValue($optionsId);
        }

        $tenor = new Zend_Form_Element_Select('tenor');
        $tenor
            ->addMultiOptions($this->_getTenorOptions())
            ->setLabel('tenor')
        ;

        $downpaymentRate = new Zend_Form_Element_Text('downpaymentRate');
        $downpaymentRate
            ->setLabel('downpayment_rate')
        ;

        $minimalDownpayment = new Zend_Form_Element_Text('minimalDownpayment');
        $minimalDownpayment
            ->setLabel('minimal_downpayment')
        ;

        $residualValueRate = new Zend_Form_Element_Text('residualValueRate');
        $residualValueRate
            ->setLabel('residual_value_rate')
        ;

        $residualValueFixed = new Zend_Form_Element_Text('residualValueFixed');
        $residualValueFixed
            ->setLabel('residual_value_fixed')
        ;

        $residualValueRateMax = new Zend_Form_Element_Text('residualValueRateMax');
        $residualValueRateMax
            ->setLabel('residual_value_rate_max')
        ;

        $residualBasedOnGross = new Zend_Form_Element_Checkbox('residualBasedOnGross');
        $residualBasedOnGross
            ->setLabel('residual_based_on_gross')
        ;

        $active = new Zend_Form_Element_Checkbox('active');
        $active
            ->setLabel('active')
        ;

        return [
            $id,
            $leaseOptionsId,
            $tenor,
            $downpaymentRate,
            $minimalDownpayment,
            $residualValueRate,
            $residualValueFixed,
            $residualValueRateMax,
            $residualBasedOnGross,
            $active,
        ];
    }

}