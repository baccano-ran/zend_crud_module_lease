<?php

/**
 * Class Lease_Form_OptionTenor_View
 */
class Lease_Form_OptionTenor_View extends Lease_Form_BaseView
{
    public $floatFields = [
        'downpaymentRate',
        'minimalDownpayment',
        'residualValueRate',
        'residualValueFixed',
    ];

    public $hiddenFields = [
        'id',
        'leaseOptionsId',
    ];

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');
        $leaseOptionsId = new Zend_Form_Element_Hidden('leaseOptionsId');

        $tenor = new Zend_Form_Element_Note('tenor');
        $tenor
            ->setLabel('tenor')
        ;

        $downpaymentRate = new Zend_Form_Element_Note('downpaymentRate');
        $downpaymentRate
            ->setLabel('downpayment_rate')
        ;

        $minimalDownpayment = new Zend_Form_Element_Note('minimalDownpayment');
        $minimalDownpayment
            ->setLabel('minimal_downpayment')
        ;

        $residualValueRate = new Zend_Form_Element_Note('residualValueRate');
        $residualValueRate
            ->setLabel('residual_value_rate')
        ;

        $residualValueFixed = new Zend_Form_Element_Note('residualValueFixed');
        $residualValueFixed
            ->setLabel('residual_value_fixed')
        ;

        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt
            ->setLabel('updated_at')
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
        ;

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy
            ->setLabel('updated_by')
        ;

        return [
            $id,
            $leaseOptionsId,
            $tenor,
            $downpaymentRate,
            $minimalDownpayment,
            $residualValueRate,
            $residualValueFixed,
            $updatedAt,
            $updatedBy,
        ];
    }

}