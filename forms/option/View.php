<?php

/**
 * Class Lease_Form_Option_View
 */
class Lease_Form_Option_View extends Lease_Form_BaseView
{
    public $hiddenFields = ['id', 'leaseConfigurationsId'];

    public $floatFields = [
        'technicalFeeFixed',
        'technicalFeeRate',
        'minimumSalesPrice',
        'maxAgeAfterLease',
    ];

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $leaseConfigurationsId = new Zend_Form_Element_Hidden('leaseConfigurationsId');

        $name = new Zend_Form_Element_Note('name');
        $name
            ->setLabel('name')
        ;

        $technicalFeeFixed = new Zend_Form_Element_Note('technicalFeeFixed');
        $technicalFeeFixed
            ->setLabel('technical_fee_fixed')
        ;

        $technicalFeeRate = new Zend_Form_Element_Note('technicalFeeRate');
        $technicalFeeRate
            ->setLabel('technical_fee_rate')
        ;

        $vehicleCondition = new Zend_Form_Element_Note('vehicleCondition');
        $vehicleCondition
            ->setLabel('vehicle_condition')
            ->addFilter(new Lease_Form_Filter_VehicleCondition())
        ;

        $minimumSalesPrice = new Zend_Form_Element_Note('minimumSalesPrice');
        $minimumSalesPrice
            ->setLabel('minimum_sales_price')
        ;

        $maxAgeAfterLease = new Zend_Form_Element_Note('maxAgeAfterLease');
        $maxAgeAfterLease
            ->setLabel('max_age_after_lease')
        ;

        $optionTenors = new Lease_Form_Element_Grid(
            'options',
            $this->getAttrib('optionGridUrl')
        );
        $optionTenors
            ->setEditable(false)
            ->setLabel('option_tenors')
        ;

        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt
            ->setLabel('updated_at')
            ->addFilter(new Lease_Form_Filter_DateTimeFormat())
        ;

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy
            ->setLabel('updated_by')
        ;

        return [
            $id,
            $leaseConfigurationsId,
            $name,
            $vehicleCondition,
            $technicalFeeFixed,
            $technicalFeeRate,
            $minimumSalesPrice,
            $maxAgeAfterLease,
            $optionTenors,
            $updatedAt,
            $updatedBy,
        ];
    }

}