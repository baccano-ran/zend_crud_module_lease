<?php

/**
 * Class Lease_Form_Option_Create
 */
class Lease_Form_Option_Create extends Lease_Form_BaseCreate
{
    public $mainGroup = 'option';

    /** @var Lease_Service_Option */
    public $optionService;
    public $hiddenFields = ['id', 'leaseConfigurationsId'];

    public $floatFields = [
        'technicalFeeFixed',
        'technicalFeeRate',
        'minimumSalesPrice',
        'maxAgeAfterLease',
    ];

    public function initElements()
    {
        $this->optionService = new Lease_Service_Option();
        parent::initElements();
    }

    protected function _getVehicleConditions()
    {
        $interestService = new Lease_Service_Interest();
        return ['' => 'none'] + $interestService->getVehicleConditionOptions();
    }

    protected function _getCategoryOptions()
    {
        return $this->optionService->getAllCategories();
    }

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $leaseConfigurationsId = new Zend_Form_Element_Text('leaseConfigurationsId');
        if ($confId = $this->getAttrib('leaseConfigurationsId')) {
            $leaseConfigurationsId->setValue($confId);
        }

        $name = new Zend_Form_Element_Text('name');
        $name
            ->setLabel('name')
        ;

        $vehicleCondition = new Zend_Form_Element_Select('vehicleCondition');
        $vehicleCondition
            ->setMultiOptions($this->_getVehicleConditions())
            ->setLabel('vehicle_condition')
        ;

        $technicalFeeFixed = new Zend_Form_Element_Text('technicalFeeFixed');
        $technicalFeeFixed
            ->setLabel('technical_fee_fixed')
        ;

        $technicalFeeRate = new Zend_Form_Element_Text('technicalFeeRate');
        $technicalFeeRate
            ->setLabel('technical_fee_rate')
        ;

        $minimumSalesPrice = new Zend_Form_Element_Text('minimumSalesPrice');
        $minimumSalesPrice
            ->setLabel('minimum_sales_price')
        ;

        $maxAgeAfterLease = new Zend_Form_Element_Text('maxAgeAfterLease');
        $maxAgeAfterLease
            ->setLabel('max_age_after_lease')
        ;

        $categories = new BAS_Shared_Form_Lease_MultiSelect('categories');
        $categories
            ->addMultiOptions($this->_getCategoryOptions())
            ->setLabel('categories')
        ;

        return [
            $id,
            $leaseConfigurationsId,
            $name,
            $vehicleCondition,
            $technicalFeeFixed,
            $technicalFeeRate,
            $minimumSalesPrice,
            $maxAgeAfterLease,
            $categories,
        ];
    }

}