<?php
/**
 * Zend form: Option update form.
 * 
 * All form fields option update.
 * 
 * @package Form
 * @extends Lease_Form_Option_Create
 */
class Lease_Form_Option_Update extends Lease_Form_Option_Create
{
    /**
     * Get form elements.
     *
     * @return array
     */
    protected function _getElements()
    {
        $optionTenors = new Lease_Form_Element_Grid(
            'options',
            $this->getAttrib('optionGridUrl')
        );
        $optionTenors->setLabel('option_tenors');

        $optionTenors->setNewItemUrl($this->getAttrib('newItemUrl'));

        $optionMaxDownpayment = new Lease_Form_Element_Grid(
            'option_max_downpayment',
            $this->getAttrib('optionMaxDownpaymentGridUrl')
        );
        $optionMaxDownpayment->setLabel('option_max_downpayment');

        $optionMaxDownpayment->setNewItemUrl($this->getAttrib('newOptionMaxDownpaymentUrl'));

        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt->addFilter(new Lease_Form_Filter_DateTimeFormat());
        $updatedAt->setLabel('updated_at');

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy->setLabel('updated_by');

        return array_merge(parent::_getElements(), [
            $optionTenors,
            $optionMaxDownpayment,
            $updatedAt,
            $updatedBy,
        ]);
    }
}
