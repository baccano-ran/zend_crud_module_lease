<?php

/**
 * Class Lease_Form_Interbank_Create
 */
class Lease_Form_Interbank_Create extends Lease_Form_BaseCreate
{
    public $mainGroup = 'interbank';

    /**
     * @var Lease_Service_InterbankRate
     */
    public $interbankService;

    public function initElements()
    {
        $this->interbankService = new Lease_Service_InterbankRate();
        parent::initElements();
    }

    /**
     * @return array
     */
    public function getPeriodOptions()
    {
        $service = new Lease_Service_InterbankRate();
        return ['' => 'none'] + $service->getAvailablePeriods();
    }

    /**
     * @return array
     */
    protected function _getElements()
    {
        return $this->getElementBuilder()
            ->createElement('hidden', 'id', [], ['hidden'])
            ->createElement('text', 'name', ['label' => 'name'], ['required', 'string' => ['max' => 100]])
            ->createElement('text', 'rate', ['label' => 'rate'], ['float'])
            ->createElement('select', 'period', [
                'label' => 'period',
                'multiOptions' => $this->getPeriodOptions(),
            ])
            ->createElement('dateSelect', 'liborDate', ['label' => 'libor_date'])

            ->getElements()
        ;
    }

}