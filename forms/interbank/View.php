<?php

/**
 * Class Lease_Form_Interbank_View
 */
class Lease_Form_Interbank_View extends Lease_Form_BaseView
{
    public $floatFields = ['rate'];

    /**
     * @return array
     */
    protected function _getElements()
    {
        $id = new Zend_Form_Element_Hidden('id');

        $name = new Zend_Form_Element_Note('name');
        $name
            ->setLabel('name')
        ;

        $rate = new Zend_Form_Element_Note('rate');
        $rate
            ->setLabel('rate')
        ;

        $period = new Zend_Form_Element_Note('period');
        $period
            ->setLabel('period')
        ;

        $liborDate = new Zend_Form_Element_Note('liborDate');
        $liborDate
            ->setLabel('libor_date')
        ;

        $updatedAt = new Zend_Form_Element_Note('updatedAt');
        $updatedAt
            ->setLabel('updated_at')
        ;

        $updatedBy = new Zend_Form_Element_Note('updater_full_name');
        $updatedBy
            ->setLabel('updated_by')
        ;

        return [
            $id,
            $name,
            $rate,
            $period,
            $liborDate,
            $updatedAt,
            $updatedBy,
        ];
    }

}