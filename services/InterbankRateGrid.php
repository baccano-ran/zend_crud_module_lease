<?php

/**
 * Class Lease_Service_InterbankRateGrid
 */
class Lease_Service_InterbankRateGrid extends Lease_Service_BaseGrid
{
    protected function _setColumns()
    {
        $interbankService = new Lease_Service_InterbankRate();

        $this->_columns = [
            'id' => [
                'hidden' => true,
            ],
            'name' => [
                'title' => 'name',
            ],
            'rate' => [
                'title' => 'rate',
                'decorator' => $this->_getFloatDecorator()
            ],
            'period' => [
                'title' => 'period',
                'decorator' => [
                    'function' => function($rowData) use ($interbankService) {
                        return $interbankService->getPeriodName($rowData['period']);
                    }
                ]
            ],
            'libor_date' => [
                'title' => 'libor_date',
                'decorator' => $this->_getDateDecorator()
            ],
            'updated_at' => [
                'title' => 'label_updated_at',
                'decorator' => $this->_getDateTimeDecorator()
            ],
            'updater_full_name' => [
                'title' => 'label_updated_by',
            ],
        ];
    }
}