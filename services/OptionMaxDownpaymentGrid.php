<?php
/**
 * Class Lease_Service_OptionMaxDownpaymentGrid
 */
class Lease_Service_OptionMaxDownpaymentGrid extends Lease_Service_BaseGrid
{
    /**
     * Set grid columns.
     */
    protected function _setColumns()
    {
        $euroFilter = new Lease_Form_Filter_Template(['before' => '&euro; ']);
        $percentFilter = new Lease_Form_Filter_Template(['after' => ' %']);

        $this->_columns = [

            'id' => [
                'hidden' => true,
            ],

            'sales_price_min' => [
                'title' => 'sales_price_min',
                'decorator' => $this->_getDecoratorWithFilter($euroFilter),
            ],

            'sales_price_max' => [
                'title' => 'sales_price_max',
                'decorator' => $this->_getDecoratorWithFilter($euroFilter),
            ],

            'maximum_downpayment_rate' => [
                'title' => 'maximum_downpayment_rate',
                'decorator' => $this->_getDecoratorWithFilter($percentFilter),
            ],

            'archived' => [
                'title' => 'archived',
                'decorator' => $this->_getBoolDecorator(),
            ],

        ];
    }
}