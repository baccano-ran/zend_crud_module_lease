<?php

/**
 * Class Lease_Service_FeeGrid
 */
class Lease_Service_FeeGrid extends Lease_Service_BaseGrid
{
    protected function _setColumns()
    {
        $service = new Lease_Service_Fee();
        $euroFilter = new Lease_Form_Filter_Template(['before' => '&euro; ']);
        $floatFilter = new Lease_Form_Filter_Float($service->getDecimalSeparator());

        $this->_columns = [
            'id' => [
                'hidden' => true,
            ],
            'updater_abbr' => [
                'hidden' => true,
            ],
            'fee_category' => [
                'title' => 'fee_category',
            ],
            'country_name' => [
                'title' => 'label_country',
            ],
            'amount' => [
                'title' => 'amount',
                'decorator' => $this->_getDecoratorWithFilters([$floatFilter, $euroFilter]),
            ],
            'vehicle_condition' => [
                'title' => 'vehicle_condition',
                'decorator' => $this->_getVehicleConditionDecorator(),
            ],
            'active' => [
                'title' => 'active',
                'decorator' => $this->_getBoolDecorator(),
            ],
            'updated_at' => [
                'title' => 'label_updated_at',
                'decorator' => $this->_getDateTimeDecorator(),
            ],
            'updater_full_name' => [
                'title' => 'label_updated_by',
            ],
        ];
    }
}