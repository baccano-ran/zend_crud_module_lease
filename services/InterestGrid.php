<?php

/**
 * Class Lease_Service_InterestGrid
 */
class Lease_Service_InterestGrid extends Lease_Service_BaseGrid
{
    protected function _setColumns()
    {
        $this->_columns = [
            'id' => [
                'hidden' => true,
            ],
            'lir_rate' => [
                'hidden' => true,
            ],
            'country_name' => [
                'title' => 'label_country',
            ],
            'vehicle_condition' => [
                'title' => 'vehicle_condition',
                'decorator' => $this->_getVehicleConditionDecorator(),
            ],
            'lir_name' => [
                'title' => 'lease_ibor',
                'decorator' => [
                    'function' => function($rowData) {
                        return Lease_Library_Formats::format(
                            Lease_Library_Formats::INTERBANK,
                            $rowData['lir_name'],
                            $rowData['lir_rate']
                        );
                    },
                ],
            ],
            'markup' => [
                'title' => 'markup',
            ],
            'updated_at' => [
                'title' => 'label_updated_at',
                'decorator' => $this->_getDateTimeDecorator(),
            ],
            'updater_full_name' => [
                'title' => 'label_updated_by',
            ],
        ];
    }
}