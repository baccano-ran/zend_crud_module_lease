<?php

/**
 * Class Lease_Service_FeeCategoryGrid
 */
class Lease_Service_FeeCategoryGrid extends Lease_Service_BaseGrid
{
    protected function _setColumns()
    {
        $this->_columns = [
            'id' => [
                'hidden' => true,
            ],
            'name' => [
                'title' => 'name',
            ],
        ];
    }
}