<?php

/**
 * Class Lease_Service_OptionTenorGrid
 */
class Lease_Service_OptionTenorGrid extends Lease_Service_BaseGrid
{
    protected function _setColumns()
    {
        $monthFilter = new Lease_Form_Filter_Template(['after' => ' months']);
        $euroFilter = new Lease_Form_Filter_Template(['before' => '&euro; ']);
        $percentFilter = new Lease_Form_Filter_Template(['after' => ' %']);

        $this->_columns = [

            'id' => [
                'hidden' => true,
            ],

            'tenor' => [
                'title' => 'tenor',
                'decorator' => $this->_getDecoratorWithFilter($monthFilter),
            ],

            'downpayment_rate' => [
                'title' => 'downpayment_rate',
                'decorator' => $this->_getDecoratorWithFilter($percentFilter),
            ],

            'minimal_downpayment' => [
                'title' => 'minimal_downpayment',
                'decorator' => $this->_getDecoratorWithFilter($euroFilter),
            ],

            'residual_value_rate' => [
                'title' => 'residual_value_rate',
                'decorator' => $this->_getDecoratorWithFilter($percentFilter),
            ],

            'residual_value_fixed' => [
                'title' => 'residual_value_fixed',
                'decorator' => $this->_getFloatDecorator(),
            ],

            'residual_value_rate_max' => [
                'title' => 'residual_value_rate_max',
                'decorator' => $this->_getFloatDecorator(),
            ],

            'residual_based_on_gross' => [
                'title' => 'residual_based_on_gross',
                'decorator' => $this->_getBoolDecorator(),
            ],

            'active' => [
                'title' => 'active',
                'decorator' => $this->_getBoolDecorator(),
            ],

        ];
    }
}