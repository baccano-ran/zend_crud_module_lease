<?php

/**
 * Class Lease_Service_VatRateGrid
 */
class Lease_Service_VatRateGrid extends Lease_Service_BaseGrid
{
    protected function _setColumns()
    {
        $this->_columns = [

            'id' => [
                'hidden' => true,
            ],

            'country_name' => [
                'title' => 'country_name',
            ],

            'standard_rate' => [
                'title' => 'standard_rate',
                'decorator' => $this->_getFloatDecorator(),
            ],

            'reduced_rate' => [
                'title' => 'reduced_rate',
                'decorator' => $this->_getFloatDecorator(),
            ],

            'updated_at' => [
                'title' => 'label_updated_at',
                'decorator' => $this->_getDateDecorator(),
            ],

            'updater_full_name' => [
                'title' => 'label_updated_by',
            ],

        ];
    }
}