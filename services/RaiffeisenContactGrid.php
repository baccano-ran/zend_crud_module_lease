<?php

/**
 * Class Lease_Service_RaiffeisenContactGrid
 */
class Lease_Service_RaiffeisenContactGrid extends Lease_Service_BaseGrid
{
    protected function _setColumns()
    {
        $filterMail = new Lease_Form_Filter_Template();
        $filterMail->setTemplate('<a href="mailto:{value}" class="mailto">{value}</a>');

        $this->_columns = [

            'id' => [
                'hidden' => true,
            ],

            'created_at' => [
                'hidden' => true,
            ],

            'country_code' => [
                'hidden' => true,
            ],

            'country_name' => [
                'title' => 'label_country',
            ],

            'fullname' => [
                'title' => 'fullname',
            ],

            'email' => [
                'title' => 'email',
                'decorator' => $this->_getDecoratorWithFilter($filterMail),
            ],

            'phone' => [
                'title' => 'phone',
            ],

            'mobile' => [
                'title' => 'mobile',
            ],

            'fax' => [
                'title' => 'fax',
            ],

            'updated_at' => [
                'title' => 'label_updated_at',
                'decorator' => $this->_getDateDecorator(),
            ],

            'updater_full_name' => [
                'title' => 'label_updated_by',
            ],

        ];
    }
}