<?php

/**
 * Class Lease_Service_AdministrationCostGrid
 */
class Lease_Service_AdministrationCostGrid extends Lease_Service_BaseGrid
{
    protected function _setColumns()
    {
        $service = new Lease_Service_AdministrationCost();
        $euroFilter = new Lease_Form_Filter_Template(['before' => '&euro; ']);
        $floatFilter = new Lease_Form_Filter_Float($service->getDecimalSeparator());

        $this->_columns = [
            'id' => [
                'hidden' => true,
            ],
            'country_name' => [
                'title' => 'label_country',
            ],
            'fixed' => [
                'title' => 'fixed',
                'decorator' => $this->_getDecoratorWithFilters([$floatFilter, $euroFilter]),
            ],
            'rate' => [
                'title' => 'rate',
                'decorator' => $this->_getFloatDecorator(),
            ],
            'updated_at' => [
                'title' => 'label_updated_at',
                'decorator' => $this->_getDateTimeDecorator(),
            ],
            'updater_full_name' => [
                'title' => 'label_updated_by',
            ],
        ];
    }
}