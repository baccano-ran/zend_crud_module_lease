<?php

/**
 * Class Lease_Service_OfferEmailTemplateGrid
 */
class Lease_Service_OfferEmailTemplateGrid extends Lease_Service_BaseGrid
{
    protected function _setColumns()
    {
        $this->_columns = [
            'id' => [
                'hidden' => true,
            ],
            'country_name' => [
                'title' => 'label_country',
            ],
            'subject' => [
                'title' => 'subject',
            ],
            'version' => [
                'title' => 'version',
            ],
            'active' => [
                'title' => 'active',
                'decorator' => $this->_getBoolDecorator(),
            ],
            'created_at' => [
                'title' => 'label_created_at',
                'decorator' => $this->_getDateTimeDecorator(),
            ],
            'creator_full_name' => [
                'title' => 'label_created_by',
            ],
            'updated_at' => [
                'title' => 'label_updated_at',
                'decorator' => $this->_getDateTimeDecorator(),
            ],
            'updater_full_name' => [
                'title' => 'label_updated_by',
            ],
        ];
    }
}