<?php

/**
 * Class Lease_Service_ConfigurationGrid
 */
class Lease_Service_ConfigurationGrid extends Lease_Service_BaseGrid
{
    protected function _setColumns()
    {
        $this->_columns = [

            'id' => [
                'hidden' => true,
            ],

            'country_name' => [
                'title' => 'label_country',
            ],

            'calculate_in_prenumerando' => [
                'title' => 'calculate_in_prenumerando',
                'decorator' => $this->_getBoolDecorator(),
            ],

            'calculate_with_residual_value' => [
                'title' => 'calculate_with_residual_value',
                'decorator' => $this->_getBoolDecorator(),
            ],

            'currency' => [
                'title' => 'currency',
            ],

            'contact_form_recipients' => [
                'title' => 'contact_form_recipients',
            ],

            'apply_form_recipients' => [
                'title' => 'apply_form_recipients',
            ],

            'inquiry_register_ticket' => [
                'title' => 'inquiry_register_ticket',
                'decorator' => $this->_getBoolDecorator(),
            ],

            'ticket_source_name_contact' => [
                'title' => 'ticket_source_name_contact',
            ],

            'ticket_source_name_apply' => [
                'title' => 'ticket_source_name_apply',
            ],

            'minimum_lease_price' => [
                'title' => 'minimum_lease_price',
                'decorator' => $this->_getFloatDecorator(),
            ],

            'minimum_sales_price' => [
                'title' => 'minimum_sales_price',
                'decorator' => $this->_getFloatDecorator(),
            ],

            'max_age_after_lease' => [
                'title' => 'max_age_after_lease',
                'decorator' => $this->_getFloatDecorator(),
            ],

            'max_age_start_lease' => [
                'title' => 'max_age_start_lease',
                'decorator' => $this->_getFloatDecorator(),
            ],

            'updated_at' => [
                'title' => 'label_updated_at',
                'decorator' => $this->_getDateTimeDecorator(),
            ],

            'updater_full_name' => [
                'title' => 'label_updated_by',
            ],

            'active' => [
                'title' => 'active',
                'decorator' => $this->_getBoolDecorator(),
            ],

        ];
    }
}