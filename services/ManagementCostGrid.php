<?php

/**
 * Class Lease_Service_ManagementCostGrid
 */
class Lease_Service_ManagementCostGrid extends Lease_Service_BaseGrid
{
    protected function _setColumns()
    {
        $this->_columns = [
            'id' => [
                'hidden' => true,
            ],
            'country_name' => [
                'title' => 'label_country',
            ],
            'rate' => [
                'title' => 'rate',
                'decorator' => $this->_getFloatDecorator(),
            ],
            'updated_at' => [
                'title' => 'label_updated_at',
                'decorator' => $this->_getDateTimeDecorator(),
            ],
            'updater_full_name' => [
                'title' => 'label_updated_by',
            ]
        ];
    }
}
