<?php

/**
 * Class Lease_Service_PriceGrid
 */
class Lease_Service_PriceGrid extends Lease_Service_BaseGrid
{
    public $idColumns = ['legacy_VoertuigID', 'country_code', 'tenor'];

    protected function _setColumns()
    {
        $this->_columns = [
            'country_code' => [
                'hidden' => true,
            ],
            'legacy_VoertuigID' => [
                'title' => 'legacy_voertuig_id',
            ],
            'country_name' => [
                'title' => 'label_country',
            ],
            'tenor' => [
                'title' => 'tenor',
            ],
            'legacy_TruckID' => [
                'title' => 'legacy_truck_id',
            ],
            'annuity' => [
                'title' => 'annuity',
            ],
            'residual' => [
                'title' => 'residual',
            ],
            'downpayment' => [
                'title' => 'downpayment',
            ],
            'currency' => [
                'title' => 'currency',
            ],
            'original_annuity' => [
                'title' => 'original_annuity',
            ],
            'original_residual' => [
                'title' => 'original_residual',
            ],
            'original_downpayment' => [
                'title' => 'original_downpayment',
            ],
            'recalculated_on' => [
                'title' => 'recalculated_on',
                'decorator' => $this->_getDateTimeDecorator(),
            ],
            'matched_option_name' => [
                'title' => 'matched_option_name',
            ],
        ];
    }
    
    protected function _setFilters()
    {
        $service = new Lease_Service_BaseService();
        $countries = $service->getCountryOptions();
        $countries = array_combine($countries, $countries);

        $this->_filters = [
            'legacy_VoertuigID' => [
                'searchType' => '=',
            ],
            'country_name' => [
                'values' => $countries,
            ],
            'tenor' => [
                'searchType' => '=',
            ],
            'legacy_TruckID' => [
                'searchType' => '=',
            ]
        ];
    }
}