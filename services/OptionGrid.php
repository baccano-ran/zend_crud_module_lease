<?php

/**
 * Class Lease_Service_OptionGrid
 */
class Lease_Service_OptionGrid extends Lease_Service_BaseGrid
{
    protected function _setColumns()
    {
        $this->_columns = [

            'id' => [
                'hidden' => true,
            ],

            'name' => [
                'title' => 'name',
            ],

            'vehicle_condition' => [
                'title' => 'vehicle_condition',
                'decorator' => $this->_getVehicleConditionDecorator(),
            ],

            'technical_fee_fixed' => [
                'title' => 'technical_fee_fixed',
                'decorator' => $this->_getFloatDecorator(),
            ],

            'technical_fee_rate' => [
                'title' => 'technical_fee_rate',
                'decorator' => $this->_getFloatDecorator(),
            ],

            'minimum_sales_price' => [
                'title' => 'minimum_sales_price',
                'decorator' => $this->_getFloatDecorator(),
            ],

            'max_age_after_lease' => [
                'title' => 'max_age_after_lease',
                'decorator' => $this->_getFloatDecorator(),
            ],

            'updated_at' => [
                'title' => 'label_updated_at',
                'decorator' => $this->_getDateTimeDecorator(),
            ],

            'updater_full_name' => [
                'title' => 'label_updated_by',
            ],

        ];
    }
}