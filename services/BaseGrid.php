<?php

/**
 * Class Lease_Service_BaseGrid
 */
class Lease_Service_BaseGrid extends Lease_Service_GridAbstract
{
    public $actionUrls;
    public $idColumns = ['id'];
    private $cacheDecorators = [];

    /**
     * Lease_Service_BaseGrid constructor.
     * @param Zend_Db_Select $source
     * @param Zend_View_Interface|null $view
     * @param array $actionUrls
     */
    public function __construct(Zend_Db_Select $source, Zend_View_Interface $view = null, $actionUrls = [])
    {
        $this->actionUrls = $actionUrls;
        parent::__construct($source, $view);
    }

    protected function _init()
    {
        parent::_init();

        $extraColumn = $this->_getActionsColumn();

        if ($extraColumn instanceof Bvb_Grid_Extra_Column) {
            $this->setExtraColumns([ $extraColumn ]);
        }
    }

    /**
     * @return Bvb_Grid_Extra_Column
     */
    protected function _getActionsColumn()
    {
        $column = new Bvb_Grid_Extra_Column();
        $column
            ->position('right')
            ->name('actions')
            ->title('')
            ->decorator($this->_view->partial(
                '_partial/columnActions.phtml',
                null,
                [
                    'actionUrls' => $this->actionUrls,
                    'id' => $this->idColumns,
                ]
            ))
        ;

        return $column;
    }

    protected function _getBoolDecorator()
    {
        if (!isset($this->cacheDecorators['bool'])) {
            $filter = new Lease_Form_Filter_YesNo();
            $view = $this->getView();
            $this->cacheDecorators['bool'] = ['function' => function($rowData) use ($filter, $view) {
                return $view->translate($filter->filter($rowData['__value']));
            }];
        }
        return $this->cacheDecorators['bool'];
    }

    protected function _getFloatDecorator()
    {
        $service = new Lease_Service_BaseService();
        return (!isset($this->cacheDecorators['float']))
            ? $this->cacheDecorators['float'] =
                $this->_getDecoratorWithFilter(new Lease_Form_Filter_Float($service->getDecimalSeparator()))
            : $this->cacheDecorators['float'];
    }

    protected function _getDateTimeDecorator()
    {
        return (!isset($this->cacheDecorators['dateTime']))
            ? $this->cacheDecorators['dateTime'] =
                $this->_getDecoratorWithFilter(new Lease_Form_Filter_DateTimeFormat())
            : $this->cacheDecorators['dateTime'];
    }

    protected function _getDateDecorator()
    {
        return (!isset($this->cacheDecorators['date']))
            ? $this->cacheDecorators['date'] =
                $this->_getDecoratorWithFilter(new Lease_Form_Filter_DateFormat())
            : $this->cacheDecorators['date'];
    }

    protected function _getVehicleConditionDecorator()
    {
        return (!isset($this->cacheDecorators['vehicleCondition']))
            ? $this->cacheDecorators['vehicleCondition'] =
                $this->_getDecoratorWithFilter(new Lease_Form_Filter_VehicleCondition())
            : $this->cacheDecorators['vehicleCondition'];
    }

    protected function _getPersonDecorator($abbreviationField = 'abbreviation')
    {
        if (!isset($this->cacheDecorators['person'])) {
            $filter = new Lease_Form_Filter_Person();
            $this->cacheDecorators['person'] = ['function' => function($rowData) use ($filter, $abbreviationField) {
                return $filter->filter($rowData['__value'], $rowData[$abbreviationField]);
            }];
        }
        return $this->cacheDecorators['person'];
    }

    protected function _getDecoratorWithFilter(Zend_Filter_Interface $filter)
    {
        return ['function' => function($rowData) use ($filter) {
            return $filter->filter($rowData['__value']);
        }];
    }

    protected function _getDecoratorWithFilters(array $filters)
    {
        return ['function' => function($rowData) use ($filters) {
            $value = $rowData['__value'];
            /** @var Zend_Filter_Interface $filter */
            foreach ($filters as $filter) {
                $value = $filter->filter($value);
            }
            return $value;
        }];
    }
}